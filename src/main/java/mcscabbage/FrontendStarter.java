package mcscabbage;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mcscabbage.controller.common.preferences.Preferences;
import mcscabbage.controller.common.viewrouter.ViewRouter;

import java.util.ResourceBundle;

public class FrontendStarter extends Application {
    public static void start() {
        launch();
    }

    public void sceneSetUp(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(ViewRouter.class.getResource("/gui/menu/Menu.fxml"));
        loadBundle(fxmlLoader);
        Parent root = fxmlLoader.load();
        Scene scene = new Scene(root, 1280, 720);
        primaryStage.setTitle("MCSCabbage");
        primaryStage.setScene(scene);
        //primaryStage.setResizable(false);
        //primaryStage.setFullScreen(true);
        primaryStage.show();
    }


    @Override
    public void start(Stage primaryStage) throws Exception {
        sceneSetUp(primaryStage);
    }

    private void loadBundle(FXMLLoader loader) {
        Preferences preferences = new Preferences();
        preferences.load();

        loader.setResources(ResourceBundle.getBundle("bundle/language", preferences.getLocale()));
    }
}
