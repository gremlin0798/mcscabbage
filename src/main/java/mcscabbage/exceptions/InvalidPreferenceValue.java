package mcscabbage.exceptions;

public class InvalidPreferenceValue extends Exception {
    public InvalidPreferenceValue(String message) {
        super(message);
    }
}
