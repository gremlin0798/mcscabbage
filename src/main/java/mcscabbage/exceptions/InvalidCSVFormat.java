package mcscabbage.exceptions;

public class InvalidCSVFormat extends Exception {
    public InvalidCSVFormat(String message) {
        super(message);
    }
}
