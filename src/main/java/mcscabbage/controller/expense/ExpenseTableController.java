package mcscabbage.controller.expense;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import mcscabbage.controller.common.DialogGenerator;
import mcscabbage.controller.common.TableViewUtil;
import mcscabbage.controller.common.viewrouter.FXMLView;
import mcscabbage.controller.common.viewrouter.ViewRouter;
import mcscabbage.entity.Expense;
import mcscabbage.service.ExpenseService;

import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

public class ExpenseTableController extends FXMLView implements Initializable {
    @FXML
    private TableView<Expense> dataTable;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        dataTable.getColumns().addAll(TableViewUtil.generateColumns(Arrays.asList("date", "name", "description", "value")));

        ExpenseService.getExpenses().forEach(e -> dataTable.getItems().addAll(e));
    }

    public void onAddButton() {
        ViewRouter.loadView(ExpenseAddController.class);
    }

    public void onEditButton() {
        Expense selectedItem = dataTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            ViewRouter.loadView(ExpenseEditController.class, selectedItem);
        } else DialogGenerator.showErrorDialog("Error", "Select row first.", "");
    }

    public void onDeleteButton() {
        Expense expense = dataTable.getSelectionModel().getSelectedItem();

        if (expense != null) {
            ExpenseService.deleteExpense(expense);
            ViewRouter.loadView(ExpenseTableController.class);
        } else DialogGenerator.showErrorDialog("Error", "Select row first.", "");
    }
}
