package mcscabbage.controller.expense;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import mcscabbage.controller.common.DialogGenerator;
import mcscabbage.controller.common.FormUtil;
import mcscabbage.controller.common.viewrouter.FXMLViewWithParameter;
import mcscabbage.controller.common.viewrouter.ViewRouter;
import mcscabbage.entity.Expense;
import mcscabbage.service.ExpenseService;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class ExpenseEditController extends FXMLViewWithParameter<Expense> implements Initializable {
    @FXML
    private DatePicker datePicker;

    @FXML
    private TextField nameInputField;

    @FXML
    private TextField descriptionInputField;

    @FXML
    private TextField valueInputField;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        final String pattern = "yyyy-MM-dd";
        datePicker.setPromptText(pattern.toLowerCase());

        datePicker.setConverter(FormUtil.dateFormatter(pattern));

        nameInputField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty())
                nameInputField.setStyle("-fx-control-inner-background: #FF0000");
            else nameInputField.setStyle("-fx-control-inner-background: #FFFFFF");
        });

        valueInputField.setTextFormatter(FormUtil.doubleFormatter());
    }


    @Override
    public void updateValues() {
        datePicker.setValue(super.getData().getDate());
        nameInputField.setText(super.getData().getName());
        descriptionInputField.setText(super.getData().getDescription());
        valueInputField.setText(Double.toString(super.getData().getValue()));
    }

    public void onSaveButton() {
        if (isFormValid()) {
            ExpenseService.updateExpense(super.getData(),
                    new Expense(datePicker.getValue(),
                            nameInputField.getText(),
                            descriptionInputField.getText(),
                            Double.parseDouble(valueInputField.getText())));
            ViewRouter.loadView(ExpenseTableController.class);
        }
    }

    public void onBackButton() {
        ViewRouter.loadView(ExpenseTableController.class);
    }

    private boolean isFormValid() {
        Map<String, String> errors = new HashMap<>();
        if (datePicker.getValue() == null) errors.put("Date", "Cant be empty");
        if (nameInputField.getText().isEmpty()) errors.put("Name", "Cant be empty");

        if (errors.isEmpty()) return true;
        else {
            DialogGenerator.formErrorDialog(errors);
            return false;
        }
    }
}
