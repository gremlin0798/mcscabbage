package mcscabbage.controller.salary;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import mcscabbage.controller.common.DialogGenerator;
import mcscabbage.controller.common.TableViewUtil;
import mcscabbage.controller.common.viewrouter.FXMLView;
import mcscabbage.controller.common.viewrouter.ViewRouter;
import mcscabbage.entity.Salary;
import mcscabbage.service.SalaryService;

import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

public class SalaryTableController extends FXMLView implements Initializable {
    @FXML
    private TableView<Salary> dataTable;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        dataTable.getColumns().addAll(TableViewUtil.generateColumns(Arrays.asList("date", "name", "description", "value")));

        SalaryService.getSalaries().forEach(e -> dataTable.getItems().addAll(e));
    }

    public void onAddButton() {
        ViewRouter.loadView(SalaryAddController.class);
    }

    public void onEditButton() {
        Salary selectedItem = dataTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            ViewRouter.loadView(SalaryEditController.class, selectedItem);
        } else DialogGenerator.showErrorDialog("Error", "Select row first.", "");
    }

    public void onDeleteButton() {
        Salary salary = dataTable.getSelectionModel().getSelectedItem();

        if (salary != null) {
            SalaryService.deleteSalary(salary);
            ViewRouter.loadView(SalaryTableController.class);
        } else DialogGenerator.showErrorDialog("Error", "Select row first.", "");
    }
}
