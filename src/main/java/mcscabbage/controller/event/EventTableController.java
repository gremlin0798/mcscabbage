package mcscabbage.controller.event;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import mcscabbage.controller.common.DialogGenerator;
import mcscabbage.controller.common.TableViewUtil;
import mcscabbage.controller.common.viewrouter.FXMLView;
import mcscabbage.controller.common.viewrouter.ViewRouter;
import mcscabbage.entity.Event;
import mcscabbage.service.EventService;

import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

public class EventTableController extends FXMLView implements Initializable {

    @FXML
    private TableView<Event> dataTable;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        dataTable.getColumns().addAll(TableViewUtil.generateColumns(Arrays.asList("date", "name", "place", "company", "role", "rate", "description")));

        EventService.getEvents().forEach(e -> dataTable.getItems().addAll(e));
    }

    public void onAddButton() {
        ViewRouter.loadView(EventAddController.class);
    }

    public void onEditButton() {
        if (dataTable.getSelectionModel().getSelectedItem() != null) {
            ViewRouter.loadView(EventEditController.class, dataTable.getSelectionModel().getSelectedItem());
        } else DialogGenerator.showErrorDialog("Error", "Select row first.", "");
    }

    public void onDeleteButton() {
        Event event = dataTable.getSelectionModel().getSelectedItem();

        if (event != null) {
            EventService.deleteEvent(event);
            ViewRouter.loadView(EventTableController.class);
        } else DialogGenerator.showErrorDialog("Error", "Select row first.", "");
    }

}
