package mcscabbage.controller.event;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import mcscabbage.controller.common.DialogGenerator;
import mcscabbage.controller.common.FormUtil;
import mcscabbage.controller.common.viewrouter.FXMLViewWithParameter;
import mcscabbage.controller.common.viewrouter.ViewRouter;
import mcscabbage.entity.Company;
import mcscabbage.entity.Event;
import mcscabbage.entity.Place;
import mcscabbage.entity.Role;
import mcscabbage.service.CompanyService;
import mcscabbage.service.EventService;
import mcscabbage.service.PlaceService;
import mcscabbage.service.RoleService;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class EventEditController extends FXMLViewWithParameter<Event> implements Initializable {
    @FXML
    private DatePicker datePicker;

    @FXML
    private TextField nameInputField;

    @FXML
    private ComboBox<Place> placeComboBox;

    @FXML
    private ComboBox<Company> companyComboBox;

    @FXML
    private ComboBox<Role> roleComboBox;

    @FXML
    private TextField valueInputField;

    @FXML
    private TextField descriptionInputField;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        final String pattern = "yyyy-MM-dd";
        datePicker.setPromptText(pattern.toLowerCase());
        datePicker.setConverter(FormUtil.dateFormatter(pattern));

        nameInputField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty())
                nameInputField.setStyle("-fx-control-inner-background: #FF0000");
            else nameInputField.setStyle("-fx-control-inner-background: #FFFFFF");
        });

        valueInputField.setTextFormatter(FormUtil.doubleFormatter());

        placeComboBox.getItems().add(null);
        PlaceService.getPlaces().forEach(p -> placeComboBox.getItems().addAll(p));

        companyComboBox.getItems().add(null);
        CompanyService.getCompanies().forEach(c -> companyComboBox.getItems().addAll(c));

        roleComboBox.getItems().add(null);
        RoleService.getRoles().forEach(r -> roleComboBox.getItems().addAll(r));
    }

    @Override
    public void updateValues() {
        datePicker.setValue(super.getData().getDate());
        nameInputField.setText(super.getData().getName());
        placeComboBox.setValue(super.getData().getPlace());
        companyComboBox.setValue(super.getData().getCompany());
        roleComboBox.setValue(super.getData().getRole());
        valueInputField.setText(Double.toString(super.getData().getRate()));
        descriptionInputField.setText(super.getData().getDescription());
    }

    public void onSaveButton() {
        if (isFormValid()) {
            EventService.updateEvent(super.getData(),
                    new Event(datePicker.getValue(),
                            nameInputField.getText(),
                            placeComboBox.getValue(),
                            companyComboBox.getValue(),
                            roleComboBox.getValue(),
                            Double.parseDouble(valueInputField.getText()),
                            descriptionInputField.getText()
                    ));
            ViewRouter.loadView(EventTableController.class);
        }
    }

    public void onBackButton() {
        ViewRouter.loadView(EventTableController.class);
    }

    private boolean isFormValid() {
        Map<String, String> errors = new HashMap<>();
        if (datePicker.getValue() == null) errors.put("Date", "Cant be empty");
        if (nameInputField.getText().isEmpty()) errors.put("Name", "Cant be empty");

        if (errors.isEmpty()) return true;
        else {
            DialogGenerator.formErrorDialog(errors);
            return false;
        }
    }
}
