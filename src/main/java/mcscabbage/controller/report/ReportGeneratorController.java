package mcscabbage.controller.report;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import mcscabbage.controller.common.DialogGenerator;
import mcscabbage.controller.common.FormUtil;
import mcscabbage.controller.common.TableViewUtil;
import mcscabbage.controller.common.viewrouter.FXMLView;
import mcscabbage.controller.report.reportGenerator.ExcelFileGenerator;
import mcscabbage.entity.Event;
import mcscabbage.entity.Expense;
import mcscabbage.service.EventService;
import mcscabbage.service.ExpenseService;

import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.ResourceBundle;

public class ReportGeneratorController extends FXMLView implements Initializable {
    @FXML
    private DatePicker dateFrom;

    @FXML
    private DatePicker dateTo;

    @FXML
    private TableView<Event> eventsTable;

    @FXML
    private TableView<Expense> expensesTable;

    public ReportGeneratorController() {
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        final String pattern = "yyyy-MM-dd";
        dateFrom.setPromptText(pattern.toLowerCase());
        dateFrom.setConverter(FormUtil.dateFormatter(pattern));
        dateTo.setPromptText(pattern.toLowerCase());
        dateTo.setConverter(FormUtil.dateFormatter(pattern));

        eventsTable.getColumns().addAll(TableViewUtil.generateColumns(Arrays.asList("date", "name", "place", "company", "role", "rate", "description")));

        expensesTable.getColumns().addAll(TableViewUtil.generateColumns(Arrays.asList("date", "name", "description", "value")));

        dateFrom.valueProperty().addListener((ov, oldValue, newValue) -> onDateChange(newValue, dateTo.getValue()));

        dateTo.valueProperty().addListener((ov, oldValue, newValue) -> onDateChange(dateFrom.getValue(), newValue));
    }

    private void onDateChange(LocalDate from, LocalDate to) {
        if (datePickerValidator(from, to)) {
            eventsTable.getItems().removeAll(eventsTable.getItems());
            EventService.getEvents(from, to).forEach(e -> eventsTable.getItems().addAll(e));

            expensesTable.getItems().removeAll(expensesTable.getItems());
            ExpenseService.getExpenses(from, to).forEach(e -> expensesTable.getItems().addAll(e));
        }
    }

    private boolean datePickerValidator(LocalDate from, LocalDate to) {
        if (from != null) {
            if (to != null) {
                if (from.compareTo(to) <= 0) {
                    dateTo.setStyle("-fx-control-inner-background: #FFFFFF");
                    dateFrom.setStyle("-fx-control-inner-background: #FFFFFF");
                    return true;
                } else {
                    dateTo.setStyle("-fx-control-inner-background: #FF0000");
                    dateFrom.setStyle("-fx-control-inner-background: #FF0000");
                    return false;
                }

            } else {
                dateTo.setStyle("-fx-control-inner-background: #FF0000");
                return false;
            }
        } else {
            dateFrom.setStyle("-fx-control-inner-background: #FF0000");
            return false;
        }
    }

    public void saveFile(String filePath) {
        if (datePickerValidator(dateFrom.getValue(), dateTo.getValue())) {
            ExcelFileGenerator excelFileGenerator = new ExcelFileGenerator();
            if (excelFileGenerator.generateAndSave(dateFrom.getValue(), dateTo.getValue(), filePath))
                DialogGenerator.showInformationDialog("ReportGenerator", "Report created successfully", filePath);

        } else {
            DialogGenerator.showErrorDialog("Report Generator", "Invalid date", "");
        }
    }

    public void onSaveButton() {
        FileChooser fileChooser = new FileChooser();

        fileChooser.setInitialFileName("report" + LocalDate.now() + ".xlsx");

        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("EXCEL files (*.xlsx)", "*.xlsx"),
                new FileChooser.ExtensionFilter("All files", "*.*"));

        File file = fileChooser.showSaveDialog(new Stage());
        if (file != null) {
            saveFile(file.getAbsolutePath());
        }
    }
}
