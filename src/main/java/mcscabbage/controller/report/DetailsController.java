package mcscabbage.controller.report;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import mcscabbage.controller.common.FormUtil;
import mcscabbage.controller.common.viewrouter.FXMLView;
import mcscabbage.service.EventService;

import java.net.URL;
import java.time.LocalDate;
import java.util.Map;
import java.util.ResourceBundle;

public class DetailsController extends FXMLView implements Initializable {

    @FXML
    private DatePicker fromDatePicker;

    @FXML
    private DatePicker toDatePicker;

    @FXML
    private Label sumLabel;

    @FXML
    private PieChart rolePieChart;

    @FXML
    private PieChart companyPieChart;

    @FXML
    private PieChart placePieChart;

    @FXML
    private Label hoverOverLabel;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        final String pattern = "yyyy-MM-dd";
        fromDatePicker.setPromptText(pattern.toLowerCase());
        fromDatePicker.setConverter(FormUtil.dateFormatter(pattern));
        toDatePicker.setPromptText(pattern.toLowerCase());
        toDatePicker.setConverter(FormUtil.dateFormatter(pattern));

        fromDatePicker.valueProperty().addListener((ov, oldValue, newValue) -> onDateChange(newValue, toDatePicker.getValue()));

        toDatePicker.valueProperty().addListener((ov, oldValue, newValue) -> onDateChange(fromDatePicker.getValue(), newValue));

        sumLabel.setText(EventService.getEventsRateSum().toString());

        addDateToPieChart(EventService.getEventsRateSumByRole(), rolePieChart);
        addDateToPieChart(EventService.getEventsRateSumByCompany(), companyPieChart);
        addDateToPieChart(EventService.getEventsRateSumByPlace(), placePieChart);

        addListenerToPieChart(rolePieChart);
        addListenerToPieChart(companyPieChart);
        addListenerToPieChart(placePieChart);

    }

    private void refresh(LocalDate from, LocalDate to) {
        sumLabel.setText(EventService.getEventsRateSum(from, to).toString());

        rolePieChart.getData().removeAll(rolePieChart.getData());
        addDateToPieChart(EventService.getEventsRateSumByRole(from, to), rolePieChart);
        companyPieChart.getData().removeAll(companyPieChart.getData());
        addDateToPieChart(EventService.getEventsRateSumByCompany(from, to), companyPieChart);
        placePieChart.getData().removeAll(placePieChart.getData());
        addDateToPieChart(EventService.getEventsRateSumByPlace(from, to), placePieChart);

        addListenerToPieChart(rolePieChart);
        addListenerToPieChart(companyPieChart);
        addListenerToPieChart(placePieChart);
    }

    private void addDateToPieChart(Map<String, Double> data, PieChart pieChart) {
        data.forEach((k, v) -> pieChart.getData().add(new PieChart.Data(k, v)));
    }

    private boolean datePickerValidator(LocalDate from, LocalDate to) {
        if (from != null) {
            if (to != null) {
                if (from.compareTo(to) <= 0) {
                    toDatePicker.setStyle("-fx-control-inner-background: #FFFFFF");
                    fromDatePicker.setStyle("-fx-control-inner-background: #FFFFFF");
                    return true;
                } else {
                    toDatePicker.setStyle("-fx-control-inner-background: #FF0000");
                    fromDatePicker.setStyle("-fx-control-inner-background: #FF0000");
                    return false;
                }

            } else {
                toDatePicker.setStyle("-fx-control-inner-background: #FF0000");
                return false;
            }
        } else {
            fromDatePicker.setStyle("-fx-control-inner-background: #FF0000");
            return false;
        }
    }

    private void onDateChange(LocalDate from, LocalDate to) {
        if (datePickerValidator(from, to)) {
            refresh(from, to);
        }
    }

    private void addListenerToPieChart(PieChart pieChart) {

        for (PieChart.Data data : pieChart.getData()) {
            data.getNode().addEventHandler(MouseEvent.MOUSE_PRESSED,
                    e -> {
                        hoverOverLabel.setVisible(true);
                        hoverOverLabel.setTranslateX(e.getSceneX() - hoverOverLabel.getLayoutX());
                        hoverOverLabel.setTranslateY(e.getSceneY() - hoverOverLabel.getLayoutY() - 80);
                        hoverOverLabel.setText(String.valueOf(data.getPieValue()));
                    });
        }

        for (PieChart.Data data : pieChart.getData()) {
            data.getNode().addEventHandler(MouseEvent.MOUSE_RELEASED,
                    e -> hoverOverLabel.setVisible(false));
        }
    }
}
