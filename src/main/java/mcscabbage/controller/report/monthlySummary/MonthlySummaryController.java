package mcscabbage.controller.report.monthlySummary;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import mcscabbage.controller.common.TableViewUtil;
import mcscabbage.controller.common.viewrouter.FXMLView;

import java.lang.reflect.Field;
import java.net.URL;
import java.time.YearMonth;
import java.util.*;
import java.util.stream.Collectors;

public class MonthlySummaryController extends FXMLView implements Initializable {
    @FXML
    private TableView<TableRow> table;

    @FXML
    private LineChart<String, Double> chart;

    List<TableRow> dataRows;
    @FXML
    private HBox checkBoxContainer;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        table.getColumns().addAll(TableViewUtil.generateColumns(Arrays.asList(
                "yearMonth",
                "salaryFromJobs",
                "expenses",
                "expectedSalary",
                "actualSalary",
                "actualExpectedDifference",
                "debt"
        )));

        dataRows = TableRow.generateRows();

        table.getItems().addAll(dataRows);

        chart.setLegendSide(Side.BOTTOM);
        checkBoxContainer.getChildren().addAll(createCheckboxes());
        chart.getData().addAll(createSeries(dataRows));
    }

    private void addValuesToSeries(XYChart.Series<String, Double> series, Map<YearMonth, Double> data) {
        data.forEach((k, v) -> series.getData().add(new XYChart.Data<>(k.toString(), v)));
    }

    private List<XYChart.Series<String, Double>> createSeries(List<TableRow> data) {
        List<XYChart.Series<String, Double>> seriesList = new ArrayList<>();

        for (Field field : TableRow.class.getFields()) {
            if (!field.getName().equals("yearMonth")) {
                XYChart.Series<String, Double> series = new XYChart.Series<>();
                series.setName(field.getName());
                addValuesToSeries(series, dataListToMap(data, field));
                seriesList.add(series);
            }
        }
        return seriesList;
    }

    private Map<YearMonth, Double> dataListToMap(List<TableRow> data, Field field) {
        return data.stream().collect(Collectors.toMap(e -> e.yearMonth, e -> {
            Double value = 0.0;
            try {
                value = (Double) field.get(e);
            } catch (IllegalAccessException illegalAccessException) {
                illegalAccessException.printStackTrace();
            }
            return value;
        }));
    }

    private ObservableList<CheckBox> createCheckboxes() {
        ObservableList<CheckBox> checkBoxes = FXCollections.observableArrayList();

        EventHandler<ActionEvent> eh = event -> {
            if (event.getSource() instanceof CheckBox) {
                CheckBox chk = (CheckBox) event.getSource();
                if (chk.isSelected()) {
                    addSeriesToChartByName(chk.getText());
                } else if (!chk.isSelected()) {
                    removeSeriesFromChatByName(chk.getText());
                }
            }
        };

        for (Field field : TableRow.class.getFields()) {
            if (!field.getName().equals("yearMonth")) {
                CheckBox checkBox = new CheckBox(field.getName());
                checkBox.setSelected(true);
                checkBox.setOnAction(eh);
                checkBoxes.add(checkBox);
            }
        }
        return checkBoxes;
    }

    private XYChart.Series<String, Double> getSeriesByNameFromDataRow(String name) {
        return createSeries(dataRows).stream().filter(series -> series.getName().equals(name)).findAny().get();
    }

    private void addSeriesToChartByName(String name) {
        if (chart.getData().stream().noneMatch(series -> series.getName().equals(name))) {
            chart.getData().add(getSeriesByNameFromDataRow(name));
        }
    }

    private void removeSeriesFromChatByName(String name) {
        chart.getData()
                .stream()
                .filter(series -> series.getName().equals(name)).findAny().ifPresent(seriesToRemove -> chart.getData().remove(seriesToRemove));
    }
}
