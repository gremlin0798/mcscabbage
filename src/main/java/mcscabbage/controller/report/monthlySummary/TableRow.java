package mcscabbage.controller.report.monthlySummary;

import lombok.*;
import mcscabbage.service.EventService;
import mcscabbage.service.ExpenseService;
import mcscabbage.service.SalaryService;

import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class TableRow {
    @Getter
    @Setter
    public YearMonth yearMonth;
    @Getter
    @Setter
    public Double salaryFromJobs;
    @Getter
    @Setter
    public Double expenses;
    @Getter
    @Setter
    public Double expectedSalary;
    @Getter
    @Setter
    public Double actualSalary;
    @Getter
    @Setter
    public Double actualExpectedDifference;
    @Getter
    @Setter
    public Double debt;

    public static List<TableRow> generateRows() {
        List<TableRow> rows = new ArrayList<>();

        Map<YearMonth, Double> jobsMonthly = EventService.getEventsRateSumMonthly();
        Map<YearMonth, Double> expensesMonthly = ExpenseService.getExpenseValueSumMonthly();
        Map<YearMonth, Double> salariesMonthly = SalaryService.getSumMonthly();

        YearMonth endDate = getMaxDate(jobsMonthly, expensesMonthly, salariesMonthly);
        YearMonth currentDate = getMinDate(jobsMonthly, expensesMonthly, salariesMonthly);

        while (currentDate.compareTo(endDate) <= 0) {
            rows.add(createRow(rows, jobsMonthly, expensesMonthly, salariesMonthly, currentDate));
            currentDate = currentDate.plusMonths(1);
        }

        return rows;
    }

    private static TableRow createRow(List<TableRow> rows, Map<YearMonth, Double> jobsMonthly, Map<YearMonth, Double> expensesMonthly, Map<YearMonth, Double> salariesMonthly, YearMonth currentDate) {
        Double salaryFromJobs = jobsMonthly.getOrDefault(currentDate, 0.0);
        Double expenses = expensesMonthly.getOrDefault(currentDate, 0.0);
        Double expectedSalary = salaryFromJobs + expenses;
        Double actualSalary = salariesMonthly.getOrDefault(currentDate, 0.0);
        Double actualExpectedDifference = actualSalary - expectedSalary;
        Double debt;

        if (rows.isEmpty()) debt = 0.0;
        else debt = rows.get(rows.size() - 1).debt + actualExpectedDifference;

        return new TableRow(
                currentDate,
                salaryFromJobs,
                expenses,
                expectedSalary,
                actualSalary,
                actualExpectedDifference,
                debt
        );
    }

    private static YearMonth getMinDate(Map<YearMonth, Double> jobs, Map<YearMonth, Double> expenses, Map<YearMonth, Double> salaries) {
        return Stream.of(jobs.entrySet(), expenses.entrySet(), salaries.entrySet())
                .flatMap(Set::stream)
                .map(Map.Entry::getKey)
                .min(YearMonth::compareTo).orElse(YearMonth.now());
    }

    private static YearMonth getMaxDate(Map<YearMonth, Double> jobs, Map<YearMonth, Double> expenses, Map<YearMonth, Double> salaries) {
        return Stream.of(jobs.entrySet(), expenses.entrySet(), salaries.entrySet())
                .flatMap(Set::stream)
                .map(Map.Entry::getKey)
                .max(YearMonth::compareTo).orElse(YearMonth.now());
    }
}
