package mcscabbage.controller.edit;


import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;
import mcscabbage.controller.common.dialogloader.DialogView;
import mcscabbage.controller.common.preferences.Preferences;
import mcscabbage.controller.common.preferences.ValidPreferencesValues;
import mcscabbage.exceptions.InvalidPreferenceValue;

import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

public class PreferencesController extends DialogView implements Initializable {

    @FXML
    private ComboBox<Locale> localisationCombo;

    @FXML
    void onBackButton() {
        Stage stage = (Stage) localisationCombo.getScene().getWindow();

        stage.close();
    }

    @FXML
    void onSaveButton() throws InvalidPreferenceValue {
        Preferences preferences = new Preferences();
        preferences.setLanguage(localisationCombo.getValue());

        preferences.save();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Preferences preferences = new Preferences();
        preferences.load();

        localisationCombo.getItems().addAll(ValidPreferencesValues.LOCALES);
        localisationCombo.setValue(preferences.getLocale());
    }
}