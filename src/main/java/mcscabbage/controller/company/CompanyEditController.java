package mcscabbage.controller.company;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import mcscabbage.controller.common.DialogGenerator;
import mcscabbage.controller.common.viewrouter.FXMLViewWithParameter;
import mcscabbage.controller.common.viewrouter.ViewRouter;
import mcscabbage.entity.Company;
import mcscabbage.service.CompanyService;

import java.net.URL;
import java.util.ResourceBundle;

public class CompanyEditController extends FXMLViewWithParameter<Company> implements Initializable {
    @FXML
    private TextField nameInputField;

    public void onSaveButton() {
        if (!nameInputField.getText().isEmpty()) {
            CompanyService.updateCompany(super.getData(), new Company(nameInputField.getText()));
            ViewRouter.loadView(CompanyTableController.class);
        } else {
            DialogGenerator.showErrorDialog("Form error", "Name", "Cant be empty");
        }
    }

    public void onBackButton() {
        ViewRouter.loadView(CompanyTableController.class);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        nameInputField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) nameInputField.setStyle("-fx-control-inner-background: #FF0000");
            else nameInputField.setStyle("-fx-control-inner-background: #FFFFFF");
        });
    }

    @Override
    public void updateValues() {
        nameInputField.setText(super.getData().getName());
    }
}
