package mcscabbage.controller.company;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import mcscabbage.controller.common.DialogGenerator;
import mcscabbage.controller.common.TableViewUtil;
import mcscabbage.controller.common.viewrouter.FXMLView;
import mcscabbage.controller.common.viewrouter.ViewRouter;
import mcscabbage.entity.Company;
import mcscabbage.service.CompanyService;

import java.net.URL;
import java.util.Collections;
import java.util.ResourceBundle;

public class CompanyTableController extends FXMLView implements Initializable {
    @FXML
    private TableView<Company> dataTable;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        dataTable.getColumns().addAll(TableViewUtil.generateColumns(Collections.singletonList("name")));

        CompanyService.getCompanies().forEach(company -> dataTable.getItems().addAll(company));
    }

    public void onAddButton() {
        ViewRouter.loadView(CompanyAddController.class);
    }

    public void onEditButton() {
        Company selectedItem = dataTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            ViewRouter.loadView(CompanyEditController.class, selectedItem);
        } else DialogGenerator.showErrorDialog("Error", "Select row first.", "");
    }

    public void onDeleteButton() {
        Company Company = dataTable.getSelectionModel().getSelectedItem();
        boolean isDeleteSafe;
        if (Company != null) {
            if (Company.getEvents().size() == 0) {
                isDeleteSafe = true;
            } else {
                String header = "Company: \"" + Company.getName() + "\" is assigned to " + Company.getEvents().size() + " events";
                isDeleteSafe = DialogGenerator.showConfirmationDialog("Warning", header, "Are you sure to delete it");
            }
            if (isDeleteSafe) {
                CompanyService.deleteCompany(Company);
                ViewRouter.loadView(CompanyTableController.class);
            }
        } else DialogGenerator.showErrorDialog("Error", "Select row first.", "");
    }
}
