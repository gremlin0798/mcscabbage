package mcscabbage.controller.company;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import mcscabbage.controller.common.DialogGenerator;
import mcscabbage.controller.common.viewrouter.FXMLView;
import mcscabbage.controller.common.viewrouter.ViewRouter;
import mcscabbage.entity.Company;
import mcscabbage.service.CompanyService;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class CompanyAddController extends FXMLView implements Initializable {
    @FXML
    private TextField nameInputField;

    public void onAddButton() {
        if (!nameInputField.getText().isEmpty()) {
            List<Company> companies = CompanyService.getCompanies();

            if (companies.stream().anyMatch(company -> company.getName().equals(nameInputField.getText()))) {
                DialogGenerator.showErrorDialog("Form error", "Company: " + nameInputField.getText(), "Already exist");
            } else {
                CompanyService.addCompany(new Company(nameInputField.getText()));
                ViewRouter.loadView(CompanyTableController.class);
            }
        } else {
            DialogGenerator.showErrorDialog("Form error", "Name", "Cant be empty");
        }
    }

    public void onBackButton() {
        ViewRouter.loadView(CompanyTableController.class);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        nameInputField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) nameInputField.setStyle("-fx-control-inner-background: #FF0000");
            else nameInputField.setStyle("-fx-control-inner-background: #FFFFFF");
        });
    }
}
