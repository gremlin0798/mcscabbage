package mcscabbage.controller.common.preferences;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class ValidPreferencesValues {
    public static final List<Locale> LOCALES = Arrays.asList(new Locale("pl"), Locale.ENGLISH);
}
