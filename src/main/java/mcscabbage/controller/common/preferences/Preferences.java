package mcscabbage.controller.common.preferences;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import mcscabbage.exceptions.InvalidPreferenceValue;

import java.io.*;
import java.util.Locale;

@EqualsAndHashCode
public class Preferences implements Serializable {
    private static final long serialVersionUID = 1L;

    private static final String FILE_NAME = "preferences.cfg.bin";

    @Getter
    private Locale locale;

    public Preferences() {
        loadDefault();
    }

    public void setLanguage(Locale locale) throws InvalidPreferenceValue {
        if (ValidPreferencesValues.LOCALES.contains(locale)) this.locale = locale;
        else {
            throw new InvalidPreferenceValue("language: " + locale);
        }
    }

    public void load() {
        try (ObjectInputStream input = new ObjectInputStream(new FileInputStream(FILE_NAME))) {
            copyValuesFrom((Preferences) input.readObject());

        } catch (IOException | ClassNotFoundException e) {
            loadDefault();
            save();
        }
    }

    public void loadDefault() {
        this.locale = Locale.ENGLISH;
    }

    public void save() {
        try (ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(FILE_NAME))) {
            output.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void copyValuesFrom(Preferences preferences) {
        this.locale = preferences.locale;
    }
}
