package mcscabbage.controller.common.viewrouter;

public class FXMLView {
    private static final String RESOURCES = "/gui/";

    public static <T extends FXMLView> String generateResourcePath(Class<T> viewClass) {
        String className = viewClass.getCanonicalName();
        String[] nameParts = className.split("\\.");

        String folder = nameParts[2] + "/";
        String fileName = nameParts[3].replace("Controller", "") + ".fxml";

        return RESOURCES + folder + fileName;
    }

}
