package mcscabbage.controller.common.viewrouter;

import lombok.Getter;
import lombok.Setter;

abstract public class FXMLViewWithParameter<E> extends FXMLView {
    @Getter
    @Setter
    private E data;

    abstract public void updateValues();
}
