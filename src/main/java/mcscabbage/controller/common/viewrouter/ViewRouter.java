package mcscabbage.controller.common.viewrouter;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.GridPane;
import lombok.Setter;
import mcscabbage.controller.common.preferences.Preferences;

import java.io.IOException;
import java.util.ResourceBundle;

public class ViewRouter {
    @Setter
    private static GridPane routerOutlet;
    private static Parent currentView;

    public static <T extends FXMLView> void loadView(Class<T> viewClass) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(ViewRouter.class.getResource(FXMLView.generateResourcePath(viewClass)));
            loadBundle(fxmlLoader);
            Parent newView = fxmlLoader.load();
            routerOutlet.getChildren().removeAll(currentView);
            routerOutlet.getChildren().add(newView);
            currentView = newView;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static <T extends FXMLViewWithParameter<E>, E> void loadView(Class<T> viewClass, E object) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(ViewRouter.class.getResource(FXMLView.generateResourcePath(viewClass)));
            loadBundle(fxmlLoader);
            Parent newView = fxmlLoader.load();
            routerOutlet.getChildren().removeAll(currentView);
            routerOutlet.getChildren().add(newView);

            FXMLViewWithParameter<E> controller = fxmlLoader.getController();
            controller.setData(object);
            controller.updateValues();

            currentView = newView;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void loadBundle(FXMLLoader fxmlLoader) {
        Preferences preferences = new Preferences();
        preferences.load();
        fxmlLoader.setResources(ResourceBundle.getBundle("bundle/language", preferences.getLocale()));
    }
}
