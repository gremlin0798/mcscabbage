package mcscabbage.controller.common.dialogloader;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import mcscabbage.controller.common.preferences.Preferences;
import mcscabbage.controller.common.viewrouter.FXMLView;
import mcscabbage.controller.common.viewrouter.ViewRouter;

import java.io.IOException;
import java.util.ResourceBundle;

public class DialogLoader {
    public static <T extends DialogView> void show(Class<T> dialog) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(ViewRouter.class.getResource(FXMLView.generateResourcePath(dialog)));
            loadBundle(fxmlLoader);
            Scene scene = new Scene(fxmlLoader.load(), 300, 200);
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void loadBundle(FXMLLoader fxmlLoader) {
        Preferences preferences = new Preferences();
        preferences.load();
        fxmlLoader.setResources(ResourceBundle.getBundle("bundle/language", preferences.getLocale()));
    }
}
