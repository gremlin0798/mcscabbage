package mcscabbage.controller.common;

import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.LinkedList;
import java.util.List;

public class TableViewUtil {
    public static <E> List<TableColumn<E, String>> generateColumns(List<String> columnNames) {
        List<TableColumn<E, String>> columns = new LinkedList<>();

        columnNames.forEach(sN -> columns.add(new TableColumn<>(sN)));
        columns.forEach(c -> c.setCellValueFactory(new PropertyValueFactory<>(c.getText())));

        return columns;
    }
}
