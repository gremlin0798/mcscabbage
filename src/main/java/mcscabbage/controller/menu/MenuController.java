package mcscabbage.controller.menu;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.layout.GridPane;
import mcscabbage.controller.common.DialogGenerator;
import mcscabbage.controller.common.dialogloader.DialogLoader;
import mcscabbage.controller.common.viewrouter.ViewRouter;
import mcscabbage.controller.company.CompanyTableController;
import mcscabbage.controller.edit.PreferencesController;
import mcscabbage.controller.event.EventTableController;
import mcscabbage.controller.expense.ExpenseTableController;
import mcscabbage.controller.file.ExportToCSVFileController;
import mcscabbage.controller.file.ImportFromCSVFileController;
import mcscabbage.controller.place.PlaceTableController;
import mcscabbage.controller.report.DetailsController;
import mcscabbage.controller.report.ReportGeneratorController;
import mcscabbage.controller.report.monthlySummary.MonthlySummaryController;
import mcscabbage.controller.role.RoleTableController;
import mcscabbage.controller.salary.SalaryTableController;

import java.net.URL;
import java.util.ResourceBundle;


public class MenuController implements Initializable {

    @FXML
    private GridPane routerOutlet;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        ViewRouter.setRouterOutlet(routerOutlet);
    }

    @FXML
    public void onAbout() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About");
        alert.setHeaderText(null);
        alert.setContentText("Version: 0.5\n" +
                "Autor: Miłosz Rylski");

        alert.setHeight(200);
        alert.setWidth(200);

        alert.showAndWait();
    }

    @FXML
    public void onClose() {
        Platform.exit();
    }

    @FXML
    public void onViewEvents() {
        ViewRouter.loadView(EventTableController.class);
    }

    @FXML
    public void onViewExpenses() {
        ViewRouter.loadView(ExpenseTableController.class);
    }

    @FXML
    public void onViewSalaries() {
        ViewRouter.loadView(SalaryTableController.class);
    }

    @FXML
    public void onViewPlaces() {
        ViewRouter.loadView(PlaceTableController.class);
    }

    @FXML
    public void onViewRoles() {
        ViewRouter.loadView(RoleTableController.class);
    }

    @FXML
    public void onViewCompanies() {
        ViewRouter.loadView(CompanyTableController.class);
    }

    @FXML
    public void onGenerateExcel() {
        ViewRouter.loadView(ReportGeneratorController.class);
    }

    @FXML
    public void onGeneratePDF() {
        DialogGenerator.showInformationDialog("PDF Generator", "Currently not available", "");
    }

    public void onSummary() {
        ViewRouter.loadView(MonthlySummaryController.class);
    }

    public void onDetails() {
        ViewRouter.loadView(DetailsController.class);

    }

    public void onImportFromCSV() {
        ViewRouter.loadView(ImportFromCSVFileController.class);
    }

    public void onExportToCSV() {
        ViewRouter.loadView(ExportToCSVFileController.class);
    }

    public void onPreferences() {
        DialogLoader.show(PreferencesController.class);
    }
}
