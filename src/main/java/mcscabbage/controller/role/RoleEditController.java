package mcscabbage.controller.role;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import mcscabbage.controller.common.DialogGenerator;
import mcscabbage.controller.common.viewrouter.FXMLViewWithParameter;
import mcscabbage.controller.common.viewrouter.ViewRouter;
import mcscabbage.entity.Role;
import mcscabbage.service.RoleService;

import java.net.URL;
import java.util.ResourceBundle;

public class RoleEditController extends FXMLViewWithParameter<Role> implements Initializable {

    @FXML
    private TextField nameInputField;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        nameInputField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) nameInputField.setStyle("-fx-control-inner-background: #FF0000");
            else nameInputField.setStyle("-fx-control-inner-background: #FFFFFF");
        });
    }


    public void onSaveButton() {
        if (!nameInputField.getText().isEmpty()) {
            RoleService.updateRole(super.getData(), new Role(nameInputField.getText()));
            ViewRouter.loadView(RoleTableController.class);
        } else {
            DialogGenerator.showErrorDialog("Form error", "Name", "Cant be empty");
        }
    }

    public void onBackButton() {
        ViewRouter.loadView(RoleTableController.class);
    }

    @Override
    public void updateValues() {
        nameInputField.setText(super.getData().getName());
    }
}
