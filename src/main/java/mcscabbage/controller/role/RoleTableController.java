package mcscabbage.controller.role;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import mcscabbage.controller.common.DialogGenerator;
import mcscabbage.controller.common.TableViewUtil;
import mcscabbage.controller.common.viewrouter.FXMLView;
import mcscabbage.controller.common.viewrouter.ViewRouter;
import mcscabbage.entity.Role;
import mcscabbage.service.RoleService;

import java.net.URL;
import java.util.Collections;
import java.util.ResourceBundle;

public class RoleTableController extends FXMLView implements Initializable {
    @FXML
    private TableView<Role> dataTable;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        dataTable.getColumns().addAll(TableViewUtil.generateColumns(Collections.singletonList("name")));

        RoleService.getRoles().forEach(e -> dataTable.getItems().addAll(e));
    }

    public void onAddButton() {
        ViewRouter.loadView(RoleAddController.class);
    }

    public void onEditButton() {
        Role selectedItem = dataTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            ViewRouter.loadView(RoleEditController.class, selectedItem);
        } else DialogGenerator.showErrorDialog("Error", "Select row first.", "");
    }

    public void onDeleteButton() {
        Role role = dataTable.getSelectionModel().getSelectedItem();
        boolean isDeleteSafe;
        if (role != null) {
            if (role.getEvents().size() == 0) {
                isDeleteSafe = true;
            } else {
                String header = "Role: \"" + role.getName() + "\" is assigned to " + role.getEvents().size() + " events";
                isDeleteSafe = DialogGenerator.showConfirmationDialog("Warning", header, "Are you sure to delete it");
            }
            if (isDeleteSafe) {
                RoleService.deleteRole(role);
                ViewRouter.loadView(RoleTableController.class);
            }
        } else DialogGenerator.showErrorDialog("Error", "Select row first.", "");
    }
}
