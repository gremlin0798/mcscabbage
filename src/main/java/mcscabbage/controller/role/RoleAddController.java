package mcscabbage.controller.role;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import mcscabbage.controller.common.DialogGenerator;
import mcscabbage.controller.common.viewrouter.FXMLView;
import mcscabbage.controller.common.viewrouter.ViewRouter;
import mcscabbage.entity.Role;
import mcscabbage.service.RoleService;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class RoleAddController extends FXMLView implements Initializable {

    @FXML
    private TextField nameInputField;

    public void onAddButton() {
        if (!nameInputField.getText().isEmpty()) {
            List<Role> roles = RoleService.getRoles();

            if (roles.stream().anyMatch(role -> role.getName().equals(nameInputField.getText()))) {
                DialogGenerator.showErrorDialog("Form error", "Role: " + nameInputField.getText(), "Already exist");
            } else {
                RoleService.addRole(new Role(nameInputField.getText()));
                ViewRouter.loadView(RoleTableController.class);
            }
        } else RoleService.addRole(new Role(nameInputField.getText()));
    }

    public void onBackButton() {
        ViewRouter.loadView(RoleTableController.class);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        nameInputField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) nameInputField.setStyle("-fx-control-inner-background: #FF0000");
            else nameInputField.setStyle("-fx-control-inner-background: #FFFFFF");
        });
    }
}
