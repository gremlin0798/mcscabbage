package mcscabbage.controller.file;

import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import mcscabbage.controller.common.viewrouter.FXMLView;
import mcscabbage.entity.*;
import mcscabbage.exceptions.InvalidCSVFormat;
import mcscabbage.service.*;

import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class ImportFromCSVFileController extends FXMLView implements Initializable {
    public TextFlow outputField;
    public ScrollPane scrollPane;

    public void onCompany() {
        File file = getFile("company");

        if (file != null) {
            String filePath = file.getAbsolutePath();
            try {
                addLineToOutputField("Company: Loading data from file...\n");
                List<Company> companies = CSVUtil.loadCompaniesFromCSV(CSVUtil.loadFile(filePath));

                addLineToOutputField("Company: Writing data to data base...\n");
                companies.forEach(company -> {
                    if (CompanyService.getCompanies().stream().noneMatch(companyFromService -> companyFromService.equals(company)))
                        CompanyService.addCompany(company);
                    else addLineToOutputField("Company: " + company.toString() + " exist in database");
                });

                addLineToOutputField("Company: Done.\n");

            } catch (InvalidCSVFormat invalidCSVFormat) {
                addLineToOutputField("Company: InvalidCSVFormat: " + invalidCSVFormat.getMessage() + "\n");
            }
        }
    }

    public void onEvent() {
        File file = getFile("event");

        if (file != null) {
            String filePath = file.getAbsolutePath();
            try {
                addLineToOutputField("Event: Loading data from file...\n");
                List<Event> events = CSVUtil.loadEventsFromCSV(CSVUtil.loadFile(filePath));

                addLineToOutputField("Event: Writing data to data base...\n");
                events.forEach(event -> {
                    event.setCompany(CompanyService.addCompany2(event.getCompany()));
                    event.setPlace(PlaceService.addPlace2(event.getPlace()));
                    event.setRole(RoleService.addRole2(event.getRole()));
                    if (EventService.getEvents().stream().noneMatch(eventFromService -> eventFromService.equals(event)))
                        EventService.addEvent(event);
                    else addLineToOutputField("Event: " + event.toString() + " exist in database");
                });

                addLineToOutputField("Event: Done.\n");

            } catch (InvalidCSVFormat invalidCSVFormat) {
                addLineToOutputField("Event: InvalidCSVFormat: " + invalidCSVFormat.getMessage() + "\n");
            }
        }
    }

    public void onExpense() {
        File file = getFile("expense");

        if (file != null) {
            String filePath = file.getAbsolutePath();
            try {
                addLineToOutputField("Expense: Loading data from file...\n");
                List<Expense> expenses = CSVUtil.loadExpensesFromCSV(CSVUtil.loadFile(filePath));

                addLineToOutputField("Expense: Writing data to data base...\n");
                expenses.forEach(expense -> {
                    if (ExpenseService.getExpenses().stream().noneMatch(expenseFromService -> expenseFromService.equals(expense)))
                        ExpenseService.addExpense(expense);
                    else addLineToOutputField("Expense: " + expense.toString() + " exist in database");
                });

                addLineToOutputField("Expense: Done.\n");

            } catch (InvalidCSVFormat invalidCSVFormat) {
                addLineToOutputField("Expense: InvalidCSVFormat: " + invalidCSVFormat.getMessage() + "\n");
            }
        }
    }

    public void onPlace() {
        File file = getFile("place");

        if (file != null) {
            String filePath = file.getAbsolutePath();
            try {
                addLineToOutputField("Place: Loading data from file...\n");
                List<Place> places = CSVUtil.loadPlacesFromCSV(CSVUtil.loadFile(filePath));

                addLineToOutputField("Place: Writing data to data base...\n");
                places.forEach(place -> {
                    if (PlaceService.getPlaces().stream().noneMatch(placeFromService -> placeFromService.equals(place)))
                        PlaceService.addPlace(place);
                    else addLineToOutputField("Place: " + place.toString() + " exist in database");
                });

                addLineToOutputField("Place: Done.\n");

            } catch (InvalidCSVFormat invalidCSVFormat) {
                addLineToOutputField("Place: InvalidCSVFormat: " + invalidCSVFormat.getMessage() + "\n");
            }
        }
    }

    public void onRole() {
        File file = getFile("role");

        if (file != null) {
            String filePath = file.getAbsolutePath();
            try {
                addLineToOutputField("Role: Loading data from file...\n");
                List<Role> roles = CSVUtil.loadRolesFromCSV(CSVUtil.loadFile(filePath));

                addLineToOutputField("Role: Writing data to data base...\n");
                roles.forEach(role -> {
                    if (RoleService.getRoles().stream().noneMatch(roleFromService -> roleFromService.equals(role)))
                        RoleService.addRole(role);
                    else addLineToOutputField("Role: " + role.toString() + " exist in database");
                });

                addLineToOutputField("Role: Done.\n");

            } catch (InvalidCSVFormat invalidCSVFormat) {
                addLineToOutputField("Role: InvalidCSVFormat: " + invalidCSVFormat.getMessage() + "\n");
            }
        }
    }

    public void onSalary() {
        File file = getFile("salary");

        if (file != null) {
            String filePath = file.getAbsolutePath();
            try {
                addLineToOutputField("Salary: Loading data from file...\n");
                List<Salary> salaries = CSVUtil.loadSalariesFromCSV(CSVUtil.loadFile(filePath));

                addLineToOutputField("Salary: Writing data to data base...\n");
                salaries.forEach(salary -> {
                    if (SalaryService.getSalaries().stream().noneMatch(salaryFromService -> salaryFromService.equals(salary)))
                        SalaryService.addSalary(salary);
                    else addLineToOutputField("Salary: " + salary.toString() + " exist in database");
                });

                addLineToOutputField("Salary: Done.\n");

            } catch (InvalidCSVFormat invalidCSVFormat) {
                addLineToOutputField("Salary: InvalidCSVFormat: " + invalidCSVFormat.getMessage() + "\n");
            }
        }
    }

    private File getFile(String tableName) {
        FileChooser fileChooser = new FileChooser();

        fileChooser.setInitialFileName(tableName + ".csv");

        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"),
                new FileChooser.ExtensionFilter("All files", "*.*"));

        return fileChooser.showOpenDialog(new Stage());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        outputField.setTextAlignment(TextAlignment.LEFT);
        scrollPane.vvalueProperty().bind(outputField.heightProperty());
    }

    private void addLineToOutputField(String line) {
        Text text = new Text(line);
        outputField.getChildren().add(new TextFlow(text));
        outputField.getChildren().add(new Text(System.lineSeparator()));
    }
}
