package mcscabbage.controller.file;

import mcscabbage.entity.*;
import mcscabbage.exceptions.InvalidCSVFormat;

import java.io.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class CSVUtil {

    public static List<String> exportCompaniesToCSV(List<Company> Companies) {
        List<String> CSV = new LinkedList<>();

        CSV.add(new Company().headerCSV());

        Companies.forEach(company -> CSV.add(company.toCSVLine()));

        return CSV;
    }

    public static List<Company> loadCompaniesFromCSV(List<String> csvList) throws InvalidCSVFormat {
        List<Company> Companies = new LinkedList<>();
        Iterator<String> csvListIterator = csvList.iterator();

        if (!(csvListIterator.next().equals(new Company().headerCSV()))) throw new InvalidCSVFormat("Invalid header");

        while (csvListIterator.hasNext()) {
            Company company = new Company();
            company.fromCSVLine(csvListIterator.next());
            Companies.add(company);
        }

        return Companies;
    }

    public static List<String> exportEventsToCSV(List<Event> events) {
        List<String> CSV = new LinkedList<>();

        CSV.add(new Event().headerCSV());

        events.forEach(event -> CSV.add(event.toCSVLine()));

        return CSV;
    }

    public static List<Event> loadEventsFromCSV(List<String> csvList) throws InvalidCSVFormat {
        List<Event> events = new LinkedList<>();
        Iterator<String> csvListIterator = csvList.iterator();

        if (!(csvListIterator.next().equals(new Event().headerCSV()))) throw new InvalidCSVFormat("Invalid header");

        while (csvListIterator.hasNext()) {
            Event event = new Event();
            event.fromCSVLine(csvListIterator.next());
            events.add(event);
        }

        return events;
    }

    public static List<String> exportExpensesToCSV(List<Expense> expenses) {
        List<String> CSV = new LinkedList<>();

        CSV.add(new Expense().headerCSV());

        expenses.forEach(expense -> CSV.add(expense.toCSVLine()));

        return CSV;
    }

    public static List<Expense> loadExpensesFromCSV(List<String> csvList) throws InvalidCSVFormat {
        List<Expense> expenses = new LinkedList<>();
        Iterator<String> csvListIterator = csvList.iterator();

        if (!(csvListIterator.next().equals(new Expense().headerCSV()))) throw new InvalidCSVFormat("Invalid header");

        while (csvListIterator.hasNext()) {
            Expense expense = new Expense();
            expense.fromCSVLine(csvListIterator.next());
            expenses.add(expense);
        }

        return expenses;
    }

    public static List<String> exportPlacesToCSV(List<Place> places) {
        List<String> CSV = new LinkedList<>();

        CSV.add(new Place().headerCSV());

        places.forEach(place -> CSV.add(place.toCSVLine()));

        return CSV;
    }

    public static List<Place> loadPlacesFromCSV(List<String> csvList) throws InvalidCSVFormat {
        List<Place> places = new LinkedList<>();
        Iterator<String> csvListIterator = csvList.iterator();

        if (!(csvListIterator.next().equals(new Place().headerCSV()))) throw new InvalidCSVFormat("Invalid header");

        while (csvListIterator.hasNext()) {
            Place place = new Place();
            place.fromCSVLine(csvListIterator.next());
            places.add(place);
        }

        return places;
    }

    public static List<String> exportRolesToCSV(List<Role> roles) {
        List<String> CSV = new LinkedList<>();

        CSV.add(new Role().headerCSV());

        roles.forEach(role -> CSV.add(role.toCSVLine()));

        return CSV;
    }

    public static List<Role> loadRolesFromCSV(List<String> csvList) throws InvalidCSVFormat {
        List<Role> roles = new LinkedList<>();
        Iterator<String> csvListIterator = csvList.iterator();

        if (!(csvListIterator.next().equals(new Role().headerCSV()))) throw new InvalidCSVFormat("Invalid header");

        while (csvListIterator.hasNext()) {
            Role role = new Role();
            role.fromCSVLine(csvListIterator.next());
            roles.add(role);
        }

        return roles;
    }

    public static List<String> exportSalariesToCSV(List<Salary> salaries) {
        List<String> CSV = new LinkedList<>();

        CSV.add(new Salary().headerCSV());

        salaries.forEach(salary -> CSV.add(salary.toCSVLine()));

        return CSV;
    }

    public static List<Salary> loadSalariesFromCSV(List<String> csvList) throws InvalidCSVFormat {
        List<Salary> salaries = new LinkedList<>();
        Iterator<String> csvListIterator = csvList.iterator();

        if (!csvListIterator.next().equals(new Salary().headerCSV())) throw new InvalidCSVFormat("Invalid header");

        while (csvListIterator.hasNext()) {
            Salary salary = new Salary();
            salary.fromCSVLine(csvListIterator.next());
            salaries.add(salary);
        }

        return salaries;
    }

    public static List<String> loadFile(String filePath) {
        List<String> csvList = new LinkedList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            reader.lines().forEach(csvList::add);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return csvList;
    }

    public static void writeFile(String filePath, List<String> csvList) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(filePath))) {
            for (String line : csvList) {
                writer.write(line);
                writer.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
