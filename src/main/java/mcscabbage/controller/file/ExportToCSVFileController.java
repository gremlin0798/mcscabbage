package mcscabbage.controller.file;

import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import mcscabbage.controller.common.viewrouter.FXMLView;
import mcscabbage.service.*;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class ExportToCSVFileController extends FXMLView implements Initializable {
    public TextFlow outputField;
    public ScrollPane scrollPane;

    public void onCompany() {
        File file = getFile("company");

        if (file != null) {
            String filePath = file.getAbsolutePath();
            addLineToOutputField("Company: Fetching data from database...");
            addLineToOutputField("Company: Writing data to file...");
            CSVUtil.writeFile(filePath, CSVUtil.exportCompaniesToCSV(CompanyService.getCompanies()));
        }
        addLineToOutputField("Company: Done.");
    }

    public void onEvent() {
        File file = getFile("event");

        if (file != null) {
            String filePath = file.getAbsolutePath();
            addLineToOutputField("Event: Fetching data from database...");
            addLineToOutputField("Event: Writing data to file...");
            CSVUtil.writeFile(filePath, CSVUtil.exportEventsToCSV(EventService.getEvents()));
        }
        addLineToOutputField("Event: Done.");
    }

    public void onExpense() {
        File file = getFile("expense");

        if (file != null) {
            String filePath = file.getAbsolutePath();
            addLineToOutputField("Expense: Fetching data from database...");
            addLineToOutputField("Expense: Writing data to file...");
            CSVUtil.writeFile(filePath, CSVUtil.exportExpensesToCSV(ExpenseService.getExpenses()));
        }
        addLineToOutputField("Expense: Done.");
    }

    public void onPlace() {
        File file = getFile("place");

        if (file != null) {
            String filePath = file.getAbsolutePath();
            addLineToOutputField("Place: Fetching data from database...");
            addLineToOutputField("Place: Writing data to file...");
            CSVUtil.writeFile(filePath, CSVUtil.exportPlacesToCSV(PlaceService.getPlaces()));
        }
        addLineToOutputField("Place: Done.");
    }

    public void onRole() {
        File file = getFile("role");

        if (file != null) {
            String filePath = file.getAbsolutePath();
            addLineToOutputField("Role: Fetching data from database...");
            addLineToOutputField("Role: Writing data to file...");
            CSVUtil.writeFile(filePath, CSVUtil.exportRolesToCSV(RoleService.getRoles()));
        }
        addLineToOutputField("Role: Done.");
    }

    public void onSalary() {
        File file = getFile("salary");

        if (file != null) {
            String filePath = file.getAbsolutePath();
            addLineToOutputField("Salary: Fetching data from database...");
            addLineToOutputField("Salary: Writing data to file...");
            CSVUtil.writeFile(filePath, CSVUtil.exportSalariesToCSV(SalaryService.getSalaries()));
        }
        addLineToOutputField("Salary: Done.");
    }

    private File getFile(String tableName) {
        FileChooser fileChooser = new FileChooser();

        fileChooser.setInitialFileName(tableName + ".csv");

        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv"),
                new FileChooser.ExtensionFilter("All files", "*.*"));

        return fileChooser.showSaveDialog(new Stage());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        outputField.setTextAlignment(TextAlignment.LEFT);
        scrollPane.vvalueProperty().bind(outputField.heightProperty());
    }

    private void addLineToOutputField(String line) {
        Text text = new Text(line);
        outputField.getChildren().add(new TextFlow(text));
        outputField.getChildren().add(new Text(System.lineSeparator()));
    }
}
