package mcscabbage.controller.place;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import mcscabbage.controller.common.DialogGenerator;
import mcscabbage.controller.common.TableViewUtil;
import mcscabbage.controller.common.viewrouter.FXMLView;
import mcscabbage.controller.common.viewrouter.ViewRouter;
import mcscabbage.entity.Place;
import mcscabbage.service.PlaceService;

import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

public class PlaceTableController extends FXMLView implements Initializable {
    @FXML
    private TableView<Place> dataTable;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

        dataTable.getColumns().addAll(TableViewUtil.generateColumns(Arrays.asList("name", "description")));

        PlaceService.getPlaces().forEach(e -> dataTable.getItems().addAll(e));
    }

    public void onAddButton() {
        ViewRouter.loadView(PlaceAddController.class);
    }

    public void onEditButton() {
        Place selectedItem = dataTable.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            ViewRouter.loadView(PlaceEditController.class, selectedItem);
        } else DialogGenerator.showErrorDialog("Error", "Select row first.", "");
    }

    public void onDeleteButton() {
        Place place = dataTable.getSelectionModel().getSelectedItem();
        boolean isDeleteSafe;
        if (place != null) {
            if (place.getEvents().size() == 0) {
                isDeleteSafe = true;
            } else {
                String header = "Place: \"" + place.getName() + "\" is assigned to " + place.getEvents().size() + " events";
                isDeleteSafe = DialogGenerator.showConfirmationDialog("Warning", header, "Are you sure to delete it");
            }
            if (isDeleteSafe) {
                PlaceService.deletePlace(place);
                ViewRouter.loadView(PlaceTableController.class);
            }
        } else DialogGenerator.showErrorDialog("Error", "Select row first.", "");
    }
}
