package mcscabbage.controller.place;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import mcscabbage.controller.common.DialogGenerator;
import mcscabbage.controller.common.viewrouter.FXMLViewWithParameter;
import mcscabbage.controller.common.viewrouter.ViewRouter;
import mcscabbage.entity.Place;
import mcscabbage.service.PlaceService;

import java.net.URL;
import java.util.ResourceBundle;

public class PlaceEditController extends FXMLViewWithParameter<Place> implements Initializable {
    @FXML
    private TextField nameInputField;

    @FXML
    private TextField descriptionInputField;

    public void onSaveButton() {
        if (!nameInputField.getText().isEmpty()) {
            PlaceService.updatePlace(super.getData(), new Place(nameInputField.getText(), descriptionInputField.getText()));
            ViewRouter.loadView(PlaceTableController.class);
        } else {
            DialogGenerator.showErrorDialog("Form error", "Name", "Cant be empty");
        }
    }

    public void onBackButton() {
        ViewRouter.loadView(PlaceTableController.class);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        nameInputField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) nameInputField.setStyle("-fx-control-inner-background: #FF0000");
            else nameInputField.setStyle("-fx-control-inner-background: #FFFFFF");
        });
    }

    @Override
    public void updateValues() {
        nameInputField.setText(super.getData().getName());
        descriptionInputField.setText(super.getData().getDescription());
    }
}
