package mcscabbage.controller.place;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import mcscabbage.controller.common.DialogGenerator;
import mcscabbage.controller.common.viewrouter.FXMLView;
import mcscabbage.controller.common.viewrouter.ViewRouter;
import mcscabbage.entity.Place;
import mcscabbage.service.PlaceService;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class PlaceAddController extends FXMLView implements Initializable {

    @FXML
    private TextField nameInputField;

    @FXML
    private TextField descriptionInputField;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        nameInputField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) nameInputField.setStyle("-fx-control-inner-background: #FF0000");
            else nameInputField.setStyle("-fx-control-inner-background: #FFFFFF");
        });
    }

    public void onAddButton() {
        if (!nameInputField.getText().isEmpty()) {
            List<Place> places = PlaceService.getPlaces();

            if (places.stream().anyMatch(place -> place.getName().equals(nameInputField.getText()))) {
                DialogGenerator.showErrorDialog("Form error", "Place: " + nameInputField.getText(), "Already exist");
            } else {
                PlaceService.addPlace(new Place(nameInputField.getText(), descriptionInputField.getText()));
                ViewRouter.loadView(PlaceTableController.class);
            }
        } else {
            DialogGenerator.showErrorDialog("Form error", "Name", "Cant be empty");
        }
    }

    public void onBackButton() {
        ViewRouter.loadView(PlaceTableController.class);
    }
}
