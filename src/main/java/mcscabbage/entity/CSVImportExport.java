package mcscabbage.entity;

import mcscabbage.exceptions.InvalidCSVFormat;

@SuppressWarnings("unused")
public interface CSVImportExport {
    String headerCSV();

    void fromCSVLine(String lineCSV) throws InvalidCSVFormat;

    String toCSVLine();
}
