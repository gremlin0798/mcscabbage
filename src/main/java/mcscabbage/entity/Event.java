package mcscabbage.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mcscabbage.exceptions.InvalidCSVFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.stream.Stream;

@Entity
@Table(name = "EVENTS")
@NoArgsConstructor
@EqualsAndHashCode
public class Event implements CSVImportExport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    @EqualsAndHashCode.Exclude
    private long id;

    @Getter
    @Setter
    private LocalDate date;

    @Getter
    @Setter
    private String name;

    @ManyToOne
    @Getter
    @Setter
    private Place place;

    @ManyToOne
    @Getter
    @Setter
    private Company company;

    @ManyToOne
    @Getter
    @Setter
    private Role role;

    @Getter
    @Setter
    private double rate;

    @Getter
    @Setter
    private String description;

    public Event(LocalDate date, String name, Place place, Company company, Role role, double rate, String description) {
        this.date = date;
        this.name = name;
        this.place = place;
        this.company = company;
        this.role = role;
        this.rate = rate;
        this.description = description;
    }

    @Override
    public String headerCSV() {
        return "date,name,place,company,role,rate,description";
    }

    @Override
    public void fromCSVLine(String lineCSV) throws InvalidCSVFormat {
        String[] fields = lineCSV.split(",");
        if (fields.length == 6) fields = Stream.of(fields, new String[]{""}).flatMap(Stream::of).toArray(String[]::new);
        if (fields.length == 7) {
            this.date = LocalDate.parse(fields[0]);
            this.name = fields[1];
            if (fields[2].length() > 0) this.place = new Place(fields[2], "");
            if (fields[3].length() > 0) this.company = new Company(fields[3]);
            if (fields[4].length() > 0) this.role = new Role(fields[4]);
            this.rate = Double.parseDouble(fields[5]);
            this.description = fields[6];
        } else throw new InvalidCSVFormat("Event:" + lineCSV);

    }

    @Override
    public String toCSVLine() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.date.toString()).append(",");
        stringBuilder.append(this.name).append(",");
        if (this.place != null) stringBuilder.append(this.place.getName()).append(",");
        else stringBuilder.append(",");
        if (this.company != null) stringBuilder.append(this.company.getName()).append(",");
        else stringBuilder.append(",");
        if (this.role != null) stringBuilder.append(this.role.getName()).append(",");
        else stringBuilder.append(",");
        stringBuilder.append(this.rate).append(",");
        stringBuilder.append(this.description);
        return stringBuilder.toString();
    }
}
