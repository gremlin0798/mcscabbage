package mcscabbage.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mcscabbage.exceptions.InvalidCSVFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "SALARIES")
@NoArgsConstructor
@EqualsAndHashCode
public class Salary implements CSVImportExport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    @EqualsAndHashCode.Exclude
    private long id;

    @Getter
    @Setter
    private LocalDate date;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String description;

    @Getter
    @Setter
    private double value;

    public Salary(LocalDate date, String name, String description, double value) {
        this.date = date;
        this.name = name;
        this.description = description;
        this.value = value;
    }

    @Override
    public String headerCSV() {
        return "date,name,description,value";
    }

    @Override
    public void fromCSVLine(String lineCSV) throws InvalidCSVFormat {
        String[] fields = lineCSV.split(",");
        if (fields.length == 4) {
            this.date = LocalDate.parse(fields[0]);
            this.name = fields[1];
            this.description = fields[2];
            this.value = Double.parseDouble(fields[3]);
        } else throw new InvalidCSVFormat("Company:" + lineCSV);
    }

    @Override
    public String toCSVLine() {
        return this.date.toString() + "," +
                this.name + "," +
                this.description + "," +
                this.value
                ;
    }
}
