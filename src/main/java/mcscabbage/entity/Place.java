package mcscabbage.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mcscabbage.exceptions.InvalidCSVFormat;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "PLACES")
@NoArgsConstructor
@EqualsAndHashCode
public class Place implements CSVImportExport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    @EqualsAndHashCode.Exclude
    private long id;

    @Getter
    @Setter
    @Column(unique = true)
    private String name;

    @Getter
    @Setter
    private String description;


    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "place_id")
    @Getter
    @Setter
    @EqualsAndHashCode.Exclude
    private Set<Event> events;

    public Place(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public boolean isUnique(Place place) {
        return !this.name.equals(place.getName());
    }

    @Override
    public String toString() {
        return this.getName();
    }

    @Override
    public String headerCSV() {
        return "name,description";
    }

    @Override
    public void fromCSVLine(String lineCSV) throws InvalidCSVFormat {
        String[] fields = lineCSV.split(",");
        if (fields.length == 2) {
            this.name = fields[0];
            this.description = fields[1];
        } else if (fields.length == 1) {
            this.name = fields[0];
            this.description = "";
        } else throw new InvalidCSVFormat("Company:" + lineCSV);
    }

    @Override
    public String toCSVLine() {
        return this.name + "," +
                this.description;
    }
}
