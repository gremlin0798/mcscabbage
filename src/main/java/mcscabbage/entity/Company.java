package mcscabbage.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import mcscabbage.exceptions.InvalidCSVFormat;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "COMPANIES")
@NoArgsConstructor
@EqualsAndHashCode
public class Company implements CSVImportExport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    @EqualsAndHashCode.Exclude
    private long id;

    @Getter
    @Setter
    @Column(unique = true)
    private String name;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "company_id")
    @Getter
    @Setter
    @EqualsAndHashCode.Exclude
    private Set<Event> events;

    public Company(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public String headerCSV() {
        return "name";
    }

    @Override
    public void fromCSVLine(String lineCSV) throws InvalidCSVFormat {
        String[] fields = lineCSV.split(",");
        if (fields.length == 1) {
            this.name = fields[0];
        } else throw new InvalidCSVFormat("Company:" + lineCSV);
    }

    @Override
    public String toCSVLine() {
        return this.name;
    }
}
