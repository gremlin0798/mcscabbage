package mcscabbage.service;

import mcscabbage.common.HibernateUtil;
import mcscabbage.entity.Event;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class EventService {
    public static List<Event> getEvents() {
        List<Event> events = null;
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            events = session.createQuery("FROM Event", Event.class).list();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return events;
    }

    public static List<Event> getEvents(LocalDate from, LocalDate to) {
        return getEvents().stream()
                .filter(event -> event.getDate().compareTo(from) >= 0 && event.getDate().compareTo(to) <= 0)
                .collect(Collectors.toList());
    }

    public static void addEvent(Event event) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(event);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        }
    }

    public static void updateEvent(Event oldEvent, Event newEvent) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.evict(oldEvent);
            oldEvent.setCompany(newEvent.getCompany());
            oldEvent.setDate(newEvent.getDate());
            oldEvent.setDescription(oldEvent.getDescription());
            oldEvent.setName(newEvent.getName());
            oldEvent.setPlace(newEvent.getPlace());
            oldEvent.setRate(newEvent.getRate());
            oldEvent.setRole(newEvent.getRole());
            session.update(oldEvent);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        }
    }

    public static void deleteEvent(Event Event) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.remove(Event);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        }
    }

    public static Double getEventsRateSum() {
        return getEvents().stream()
                .map(Event::getRate)
                .reduce(0.0, Double::sum);

    }

    public static Double getEventsRateSum(LocalDate from, LocalDate to) {
        return getEvents(from, to).stream()
                .filter(event -> event.getDate().compareTo(from) >= 0 && event.getDate().compareTo(to) <= 0)
                .map(Event::getRate)
                .reduce(0.0, Double::sum);
    }

    public static Map<YearMonth, Double> getEventsRateSumMonthly() {
        Map<YearMonth, Double> eventsMonthly = new HashMap<>();

        getEvents().forEach(event -> {
            YearMonth eventYM = YearMonth.from(event.getDate());
            Double eventRate = event.getRate();

            if (eventsMonthly.containsKey(eventYM))
                eventsMonthly.put(eventYM, eventsMonthly.get(eventYM) + eventRate);
            else
                eventsMonthly.put(eventYM, eventRate);
        });
        return eventsMonthly;
    }

    public static Map<String, Double> getEventsRateSumByRole() {
        Map<String, Double> eventsByRole = new HashMap<>();

        EventService.getEvents().forEach(event -> {
            String eventRole = "Other";
            if (Optional.ofNullable(event.getRole()).isPresent()) eventRole = event.getRole().getName();
            Double eventRate = event.getRate();

            if (eventsByRole.containsKey(eventRole))
                eventsByRole.put(eventRole, eventsByRole.get(eventRole) + eventRate);
            else
                eventsByRole.put(eventRole, eventRate);
        });

        return eventsByRole;
    }

    public static Map<String, Double> getEventsRateSumByRole(LocalDate from, LocalDate to) {
        Map<String, Double> eventsByRole = new HashMap<>();

        EventService.getEvents(from, to)
                .forEach(event -> {
                    String eventRole = "Other";
                    if (Optional.ofNullable(event.getRole()).isPresent()) eventRole = event.getRole().getName();
                    Double eventRate = event.getRate();

                    if (eventsByRole.containsKey(eventRole))
                        eventsByRole.put(eventRole, eventsByRole.get(eventRole) + eventRate);
                    else
                        eventsByRole.put(eventRole, eventRate);
                });

        return eventsByRole;
    }

    public static Map<String, Double> getEventsRateSumByCompany() {
        Map<String, Double> eventsByCompany = new HashMap<>();

        EventService.getEvents().forEach(event -> {
            String eventCompany = "Other";
            if (Optional.ofNullable(event.getCompany()).isPresent()) eventCompany = event.getCompany().getName();
            Double eventRate = event.getRate();

            if (eventsByCompany.containsKey(eventCompany))
                eventsByCompany.put(eventCompany, eventsByCompany.get(eventCompany) + eventRate);
            else
                eventsByCompany.put(eventCompany, eventRate);
        });

        return eventsByCompany;
    }

    public static Map<String, Double> getEventsRateSumByCompany(LocalDate from, LocalDate to) {
        Map<String, Double> eventsByCompany = new HashMap<>();

        EventService.getEvents(from, to)
                .forEach(event -> {
                    String eventCompany = "Other";
                    if (Optional.ofNullable(event.getCompany()).isPresent())
                        eventCompany = event.getCompany().getName();
                    Double eventRate = event.getRate();

                    if (eventsByCompany.containsKey(eventCompany))
                        eventsByCompany.put(eventCompany, eventsByCompany.get(eventCompany) + eventRate);
                    else
                        eventsByCompany.put(eventCompany, eventRate);
                });

        return eventsByCompany;
    }

    public static Map<String, Double> getEventsRateSumByPlace() {
        Map<String, Double> eventsByPlace = new HashMap<>();

        EventService.getEvents().forEach(event -> {
            String eventPlace = "Other";
            if (Optional.ofNullable(event.getPlace()).isPresent()) eventPlace = event.getPlace().getName();
            Double eventRate = event.getRate();

            if (eventsByPlace.containsKey(eventPlace))
                eventsByPlace.put(eventPlace, eventsByPlace.get(eventPlace) + eventRate);
            else
                eventsByPlace.put(eventPlace, eventRate);
        });

        return eventsByPlace;
    }

    static public Map<String, Double> getEventsRateSumByPlace(LocalDate from, LocalDate to) {
        Map<String, Double> eventsByPlace = new HashMap<>();

        EventService.getEvents(from, to)
                .forEach(event -> {
                    String eventPlace = "Other";
                    if (Optional.ofNullable(event.getPlace()).isPresent()) eventPlace = event.getPlace().getName();
                    Double eventRate = event.getRate();

                    if (eventsByPlace.containsKey(eventPlace))
                        eventsByPlace.put(eventPlace, eventsByPlace.get(eventPlace) + eventRate);
                    else
                        eventsByPlace.put(eventPlace, eventRate);
                });

        return eventsByPlace;
    }
}
