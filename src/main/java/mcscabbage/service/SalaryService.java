package mcscabbage.service;

import mcscabbage.common.HibernateUtil;
import mcscabbage.entity.Salary;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.time.YearMonth;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SalaryService {
    public static List<Salary> getSalaries() {
        List<Salary> salaries = null;
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            salaries = session.createQuery("FROM Salary", Salary.class).list();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return salaries;
    }

    public static void addSalary(Salary salary) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(salary);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        }
    }

    public static void updateSalary(Salary oldSalary, Salary newSalary) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.evict(oldSalary);
            oldSalary.setDate(newSalary.getDate());
            oldSalary.setName(newSalary.getName());
            oldSalary.setDescription(newSalary.getDescription());
            oldSalary.setValue(newSalary.getValue());
            session.update(oldSalary);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        }
    }

    public static void deleteSalary(Salary salary) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.remove(salary);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        }
    }

    public static Double salarySum() {
        return getSalaries().stream()
                .map(Salary::getValue)
                .reduce(0.0, Double::sum);
    }

    public static Map<YearMonth, Double> getSumMonthly() {
        Map<YearMonth, Double> sumMonthly = new HashMap<>();

        getSalaries().forEach(salary -> {
            YearMonth salaryYM = YearMonth.from(salary.getDate());
            Double salaryV = salary.getValue();

            if (sumMonthly.containsKey(salaryYM))
                sumMonthly.put(salaryYM, sumMonthly.get(salaryYM) + salaryV);
            else
                sumMonthly.put(salaryYM, salaryV);
        });

        return sumMonthly;
    }
}

