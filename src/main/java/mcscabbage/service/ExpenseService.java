package mcscabbage.service;

import mcscabbage.common.HibernateUtil;
import mcscabbage.entity.Expense;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ExpenseService {
    public static List<Expense> getExpenses() {
        List<Expense> expenses = null;
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            expenses = session.createQuery("FROM Expense", Expense.class).list();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return expenses;
    }

    public static List<Expense> getExpenses(LocalDate from, LocalDate to) {
        List<Expense> expenses = getExpenses();
        return expenses.stream()
                .filter(expense -> expense.getDate().compareTo(from) >= 0 && expense.getDate().compareTo(to) <= 0)
                .collect(Collectors.toList());
    }

    public static void addExpense(Expense expense) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(expense);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        }
    }

    public static void updateExpense(Expense oldExpense, Expense newExpense) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.evict(oldExpense);
            oldExpense.setDate(newExpense.getDate());
            oldExpense.setName(newExpense.getName());
            oldExpense.setDescription(newExpense.getDescription());
            oldExpense.setValue(newExpense.getValue());
            session.update(oldExpense);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        }
    }

    public static void deleteExpense(Expense expense) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.remove(expense);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        }
    }

    public static Double getExpenseValueSum() {
        return getExpenses().stream()
                .map(Expense::getValue)
                .reduce(0.0, Double::sum);
    }

    public static Map<YearMonth, Double> getExpenseValueSumMonthly() {
        Map<YearMonth, Double> expensesMonthly = new HashMap<>();

        getExpenses().forEach(event -> {
            YearMonth expenseYM = YearMonth.from(event.getDate());
            Double expenseValue = event.getValue();

            if (expensesMonthly.containsKey(expenseYM))
                expensesMonthly.put(expenseYM, expensesMonthly.get(expenseYM) + expenseValue);
            else
                expensesMonthly.put(expenseYM, expenseValue);
        });
        return expensesMonthly;
    }
}
