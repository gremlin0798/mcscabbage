package mcscabbage.service;

import mcscabbage.common.HibernateUtil;
import mcscabbage.entity.Place;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Optional;

public class PlaceService {
    public static List<Place> getPlaces() {
        List<Place> places = null;
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            places = session.createQuery("FROM Place", Place.class).list();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return places;
    }

    public static void addPlace(Place place) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(place);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        }
    }

    public static Place addPlace2(Place place) {
        if (place != null) {
            Optional<Place> placeOptional = getPlaces().stream().filter(place1 -> !place1.isUnique(place)).findFirst();
            if (placeOptional.isPresent()) return placeOptional.get();

            Transaction transaction = null;

            try (Session session = HibernateUtil.getSessionFactory().openSession()) {
                transaction = session.beginTransaction();
                session.save(place);
                transaction.commit();
            } catch (Exception e) {
                if (transaction != null) transaction.rollback();
                e.printStackTrace();
            }
        }
        return place;
    }

    public static void updatePlace(Place oldPlace, Place newPlace) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.evict(oldPlace);
            oldPlace.setName(newPlace.getName());
            oldPlace.setDescription(newPlace.getDescription());
            session.update(oldPlace);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        }
    }

    public static void deletePlace(Place place) {
        Transaction transaction = null;

        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.remove(place);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) transaction.rollback();
            e.printStackTrace();
        }
    }
}
