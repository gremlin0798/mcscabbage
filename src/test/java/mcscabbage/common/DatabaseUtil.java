package mcscabbage.common;

import mcscabbage.entity.*;
import mcscabbage.service.*;

import java.time.LocalDate;
import java.time.YearMonth;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DatabaseUtil {
    private static final LocalDate START_DATE = LocalDate.parse("2020-01-01");
    private static final LocalDate END_DATE = LocalDate.parse("2020-04-01");
    private static final int EXPENSES_DECIMATION = 3;
    private static final int EXPENSES_START_VALUE = 50;

    private static final int PLACES_NUMBER = 12;
    private static final int ROLES_NUMBER = 10;
    private static final int COMPANIES_NUMBER = 8;

    private static final int EVENTS_START_RATE = 100;

    public static void setUpDB() {
        setUpPlaces();
        setUpRoles();
        setUpCompanies();
        setUpEvents();
        setUpExpenses();
        setUpSalaries();
    }

    private static void setUpExpenses() {
        int i = 0;
        while (START_DATE.plusDays(i).compareTo(END_DATE) < 0) {
            if (i % EXPENSES_DECIMATION == 0) {
                Expense expense = new Expense(START_DATE.plusDays(i), "name" + i, "desc" + i, EXPENSES_START_VALUE + i);

                ExpenseService.addExpense(expense);
            }
            i++;
        }
    }

    private static void setUpPlaces() {
        for (int i = 0; i <= PLACES_NUMBER; i++) {
            Place place1 = new Place("place" + i, "desc" + i);

            PlaceService.addPlace(place1);
        }
    }

    private static void setUpSalaries() {
        Map<YearMonth, Double> eventsRateSumMonthly = EventService.getEventsRateSumMonthly();
        Map<YearMonth, Double> expensesValueSumMonthly = ExpenseService.getExpenseValueSumMonthly();

        Map<YearMonth, Double> mergedEventsAndExpenses = new HashMap<>(eventsRateSumMonthly);

        expensesValueSumMonthly.forEach(
                (key, value) -> mergedEventsAndExpenses.merge(key, value, Double::sum)
        );

        YearMonth maxYearMonth = mergedEventsAndExpenses.keySet().stream()
                .reduce(YearMonth.of(1995, 1), (subtotal, element) -> {
                    if (subtotal.compareTo(element) < 0) return element;
                    return subtotal;
                });
        mergedEventsAndExpenses.remove(maxYearMonth);

        mergedEventsAndExpenses.forEach((key, value) -> {
            Salary salary1 = new Salary(LocalDate.of(key.getYear(), key.getMonth(), 10),
                    "name" + key,
                    "des" + key,
                    value);

            SalaryService.addSalary(salary1);
        });
    }

    private static void setUpRoles() {
        for (int i = 0; i <= ROLES_NUMBER; i++) {
            Role role1 = new Role("role" + i);

            RoleService.addRole(role1);
        }
    }

    private static void setUpCompanies() {
        for (int i = 0; i <= COMPANIES_NUMBER; i++) {
            Company company1 = new Company("company" + i);

            CompanyService.addCompany(company1);
        }
    }

    private static void setUpEvents() {
        List<Place> places = PlaceService.getPlaces();
        List<Company> companies = CompanyService.getCompanies();
        List<Role> roles = RoleService.getRoles();
        int i = 0;
        while (START_DATE.plusDays(i).compareTo(END_DATE) < 0) {
            Event event1 = new Event(START_DATE.plusDays(i),
                    "event" + i,
                    places.get(i % PLACES_NUMBER),
                    companies.get(i % COMPANIES_NUMBER),
                    roles.get(i % ROLES_NUMBER),
                    EVENTS_START_RATE + i,
                    "");
            EventService.addEvent(event1);
            i++;
        }
    }
}
