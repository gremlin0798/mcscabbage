package mcscabbage.controller.company;

import javafx.stage.Stage;
import mcscabbage.controller.GuiTest;
import mcscabbage.entity.Company;
import mcscabbage.service.CompanyService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.matcher.control.LabeledMatchers;

import java.io.IOException;

@ExtendWith(ApplicationExtension.class)
class CompanyTableControllerTest extends GuiTest {
    @Start
    void setUpGui(Stage stage) throws IOException {
        CompanyService.addCompany(new Company("company1"));
        GuiTest.setupStage(stage, CompanyTableController.class);

    }

    @AfterEach
    void tearDown() {
        truncateDataBase();
    }

    @Test
    void shouldGoToAddCompanyViewWhenAddButtonGetsPressed(FxRobot robot) {
        robot.clickOn("Add");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Add Company"));
    }

    @Test
    void shouldGoToEditCompanyViewWhenEditButtonGetsPressed(FxRobot robot) {
        robot.clickOn("company1");
        robot.clickOn("#editButton");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Edit Company"));
    }

    @Test
    void shouldReloadViewAfterDeleteButtonGetsPressed(FxRobot robot) {
        robot.clickOn("company1");
        robot.clickOn("#deleteButton");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Companies"));

        assertThatNodeIsNotOnScreen("company1", robot);
    }
}