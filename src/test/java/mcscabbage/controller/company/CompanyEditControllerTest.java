package mcscabbage.controller.company;

import javafx.stage.Stage;
import mcscabbage.controller.GuiTest;
import mcscabbage.entity.Company;
import mcscabbage.service.CompanyService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.matcher.control.LabeledMatchers;

import java.io.IOException;

@ExtendWith(ApplicationExtension.class)
class CompanyEditControllerTest extends GuiTest {
    @Start
    void setUpGui(Stage stage) throws IOException {
        CompanyService.addCompany(new Company("company1"));
        Company company = CompanyService.getCompanies().stream().findFirst().get();
        setupStage(stage, CompanyEditController.class, company);
    }

    @Test
    void shouldGoToCompaniesTableViewWhenSaveButtonGetsPressed(FxRobot robot) {
        robot.clickOn("Save");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Companies"));
    }

    @Test
    void shouldGoToCompaniesTableViewWhenBackButtonGetsPressed(FxRobot robot) {
        robot.clickOn("Back");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Companies"));
    }

    @AfterEach
    void tearDown() {
        truncateDataBase();
    }

}