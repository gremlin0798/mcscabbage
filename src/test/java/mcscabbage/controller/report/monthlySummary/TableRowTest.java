package mcscabbage.controller.report.monthlySummary;

import mcscabbage.common.DatabaseUtil;
import mcscabbage.common.HibernateUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TableRowTest {
    @BeforeEach
    void setUp() {
        HibernateUtil.getSessionFactory();
        DatabaseUtil.setUpDB();
    }

    @Test
    void shouldGenerateValidTableRowsFromTestData() {
        List<TableRow> expected = new ArrayList<>();

        expected.add(new TableRow(
                YearMonth.of(2020, 1),
                3565.0,
                715.0,
                4280.0,
                4280.0,
                0.0,
                0.0));
        expected.add(new TableRow(
                YearMonth.of(2020, 2),
                4205.0,
                855.0,
                5060.0,
                5060.0,
                0.0,
                0.0));
        expected.add(new TableRow(
                YearMonth.of(2020, 3),
                5425.0,
                1375.0,
                6800.0,
                0.0,
                -6800.0,
                -6800.0));

        assertEquals(expected, TableRow.generateRows());

    }

}