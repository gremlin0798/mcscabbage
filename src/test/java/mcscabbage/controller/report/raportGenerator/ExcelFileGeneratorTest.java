package mcscabbage.controller.report.raportGenerator;

import mcscabbage.common.DatabaseUtil;
import mcscabbage.common.HibernateUtil;
import mcscabbage.controller.report.reportGenerator.ExcelFileGenerator;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertTrue;

class ExcelFileGeneratorTest {
    static final LocalDate from = LocalDate.parse("2020-01-01");
    static final LocalDate to = LocalDate.parse("2020-02-01");

    static String filePath = "";

    @BeforeAll
    static void generateExcelFile() {
        DatabaseUtil.setUpDB();

        ExcelFileGenerator excelFileGenerator = new ExcelFileGenerator();
        File currDir = new File(".");
        String path = currDir.getAbsolutePath();
        filePath = path.substring(0, path.length() - 1) + "test.xlsx";
        excelFileGenerator.generateAndSave(from, to, filePath);
    }

    @AfterAll
    static void tearDown() {
        HibernateUtil.shutdown();
    }

//    @AfterAll
//    static void removeGeneratedExcelFile(){
//        File excelFile = new File(filePath);
//        excelFile.delete();
//    }

    @Test
    void shouldCreateFileWithValidName() {
        File excelFile = new File(filePath);
        assertTrue(excelFile.exists());
    }


}