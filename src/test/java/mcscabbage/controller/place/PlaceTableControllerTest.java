package mcscabbage.controller.place;

import javafx.stage.Stage;
import mcscabbage.controller.GuiTest;
import mcscabbage.entity.Place;
import mcscabbage.service.PlaceService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.matcher.control.LabeledMatchers;

import java.io.IOException;

@ExtendWith(ApplicationExtension.class)
class PlaceTableControllerTest extends GuiTest {
    @Start
    void setUpGui(Stage stage) throws IOException {
        PlaceService.addPlace(new Place("place1", "desc1"));

        GuiTest.setupStage(stage, PlaceTableController.class);
    }

    @AfterEach
    void tearDown() {
        truncateDataBase();
    }

    @Test
    void shouldGoToAddPlaceViewWhenAddButtonGetsPressed(FxRobot robot) {
        robot.clickOn("Add");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Add Place"));
    }

    @Test
    void shouldGoToEditPlaceViewWhenEditButtonGetsPressed(FxRobot robot) {
        robot.clickOn("place1");
        robot.clickOn("#editButton");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Edit Place"));
    }

    @Test
    void shouldReloadPlacesTableViewWhenDeleteButtonGetsPressed(FxRobot robot) {
        robot.clickOn("place1");
        robot.clickOn("Delete");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Places"));
        assertThatNodeIsNotOnScreen("place1", robot);
    }
}