package mcscabbage.controller.file;

import mcscabbage.entity.*;
import mcscabbage.exceptions.InvalidCSVFormat;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CSVUtilTest {

    private static final List<Company> COMPANIES = Arrays.asList(
            new Company("company1"),
            new Company("company2")
    );
    private static final List<String> COMPANIES_CSV = new LinkedList<>(Arrays.asList(
            "name",
            "company1",
            "company2"
    ));
    private static final List<String> COMPANIES_CSV_INV = new LinkedList<>(Arrays.asList(
            "name,inv",
            "company1",
            "company2"
    ));

    private static final List<Event> EVENTS = Arrays.asList(
            new Event(LocalDate.parse("2020-01-01"), "event1", new Place("place1", ""), new Company("company1"), new Role("role1"), 100.0, ""),
            new Event(LocalDate.parse("2020-01-02"), "event2", null, null, null, 100.1, "desc2")
    );
    private static final List<String> EVENTS_CSV = new LinkedList<>(Arrays.asList(
            "date,name,place,company,role,rate,description",
            "2020-01-01,event1,place1,company1,role1,100.0,",
            "2020-01-02,event2,,,,100.1,desc2"
    ));
    private static final List<String> EVENTS_CSV_INV = new LinkedList<>(Arrays.asList(
            "date,name,place,company,role,rate,description,inv",
            "2020-01-01,event1,place1,company1,role1,100.0,desc1",
            "2020-01-02,event2,,,,100.1,desc2"
    ));

    private static final List<Expense> EXPENSES = Arrays.asList(
            new Expense(LocalDate.parse("2020-01-01"), "expense1", "desc1", 100.0),
            new Expense(LocalDate.parse("2020-01-02"), "expense2", "", 100.1)
    );
    private static final List<String> EXPENSES_CSV = new LinkedList<>(Arrays.asList(
            "date,name,description,value",
            "2020-01-01,expense1,desc1,100.0",
            "2020-01-02,expense2,,100.1"
    ));
    private static final List<String> EXPENSES_CSV_INV = new LinkedList<>(Arrays.asList(
            "date,name,description,value,inv",
            "2020-01-01,expense1,desc1,100.0",
            "2020-01-02,expense2,,100.1"
    ));

    private static final List<Place> PLACES = Arrays.asList(
            new Place("place1", "desc1"),
            new Place("place2", "")
    );
    private static final List<String> PLACES_CSV = new LinkedList<>(Arrays.asList(
            "name,description",
            "place1,desc1",
            "place2,"
    ));
    private static final List<String> PLACES_CSV_INV = new LinkedList<>(Arrays.asList(
            "name,description.inv",
            "place1,desc1",
            "place2,"
    ));

    private static final List<Role> ROLES = Arrays.asList(
            new Role("role1"),
            new Role("role2")
    );
    private static final List<String> ROLES_CSV = new LinkedList<>(Arrays.asList(
            "name",
            "role1",
            "role2"
    ));
    private static final List<String> ROLES_CSV_INV = new LinkedList<>(Arrays.asList(
            "name,inv",
            "role1",
            "role2"
    ));

    private static final List<Salary> SALARIES = Arrays.asList(
            new Salary(LocalDate.parse("2020-01-01"), "name1", "desc1", 50.0),
            new Salary(LocalDate.parse("2020-01-02"), "name2", "", 50.1)
    );
    private static final List<String> SALARIES_CSV = new LinkedList<>(Arrays.asList(
            "date,name,description,value",
            "2020-01-01,name1,desc1,50.0",
            "2020-01-02,name2,,50.1"
    ));
    private static final List<String> SALARIES_CSV_INV = new LinkedList<>(Arrays.asList(
            "date,name,description,value,inv",
            "2020-01-01,name1,desc1,50.0",
            "2020-01-02,name2,,50.1"
    ));

    @Test
    void shouldCreateValidCSVFromCompanyList() {
        assertEquals(COMPANIES_CSV, CSVUtil.exportCompaniesToCSV(COMPANIES));
    }

    @Test
    void shouldLoadCompaniesFromCSV() throws InvalidCSVFormat {
        assertEquals(COMPANIES, CSVUtil.loadCompaniesFromCSV(COMPANIES_CSV));
    }

    @Test
    void shouldThrowExceptionForInvalidCompaniesHeader() {
        assertThrows(InvalidCSVFormat.class, () -> CSVUtil.loadCompaniesFromCSV(COMPANIES_CSV_INV));
    }

    @Test
    void shouldCreateValidCSVFromExpenseList() {
        assertEquals(EXPENSES_CSV, CSVUtil.exportExpensesToCSV(EXPENSES));
    }

    @Test
    void shouldLoadExpensesFromCSV() throws InvalidCSVFormat {
        assertEquals(EXPENSES, CSVUtil.loadExpensesFromCSV(EXPENSES_CSV));
    }

    @Test
    void shouldThrowExceptionForInvalidExpensesHeader() {
        assertThrows(InvalidCSVFormat.class, () -> CSVUtil.loadExpensesFromCSV(EXPENSES_CSV_INV));
    }

    @Test
    void shouldCreateValidCSVFromPlaceList() {
        assertEquals(PLACES_CSV, CSVUtil.exportPlacesToCSV(PLACES));
    }

    @Test
    void shouldLoadPlacesFromCSV() throws InvalidCSVFormat {
        assertEquals(PLACES, CSVUtil.loadPlacesFromCSV(PLACES_CSV));
    }

    @Test
    void shouldThrowExceptionForInvalidPlacesHeader() {
        assertThrows(InvalidCSVFormat.class, () -> CSVUtil.loadPlacesFromCSV(PLACES_CSV_INV));
    }

    @Test
    void shouldThrowExceptionForInvalidEventHeader() {
        assertThrows(InvalidCSVFormat.class, () -> CSVUtil.loadEventsFromCSV(EVENTS_CSV_INV));
    }

    @Test
    void shouldCreateValidCSVFromEventList() {
        assertEquals(EVENTS_CSV, CSVUtil.exportEventsToCSV(EVENTS));
    }

    @Test
    void shouldLoadEventsFromCSV() throws InvalidCSVFormat {
        assertEquals(EVENTS, CSVUtil.loadEventsFromCSV(EVENTS_CSV));
    }

    @Test
    void shouldCreateValidCSVFromRoleList() {
        assertEquals(ROLES_CSV, CSVUtil.exportRolesToCSV(ROLES));
    }

    @Test
    void shouldLoadRolesFromCSV() throws InvalidCSVFormat {
        assertEquals(ROLES, CSVUtil.loadRolesFromCSV(ROLES_CSV));
    }

    @Test
    void shouldThrowExceptionForInvalidRolesHeader() {
        assertThrows(InvalidCSVFormat.class, () -> CSVUtil.loadRolesFromCSV(ROLES_CSV_INV));
    }

    @Test
    void shouldCreateValidCSVFromSalaryList() {
        assertEquals(SALARIES_CSV, CSVUtil.exportSalariesToCSV(SALARIES));
    }

    @Test
    void shouldLoadSalariesFromCSV() throws InvalidCSVFormat {
        assertEquals(SALARIES, CSVUtil.loadSalariesFromCSV(SALARIES_CSV));
    }

    @Test
    void shouldThrowExceptionForInvalidSalariesHeader() {
        assertThrows(InvalidCSVFormat.class, () -> CSVUtil.loadSalariesFromCSV(SALARIES_CSV_INV));
    }
}