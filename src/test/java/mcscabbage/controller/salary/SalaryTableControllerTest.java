package mcscabbage.controller.salary;

import javafx.stage.Stage;
import mcscabbage.controller.GuiTest;
import mcscabbage.entity.Salary;
import mcscabbage.service.SalaryService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.matcher.control.LabeledMatchers;

import java.io.IOException;
import java.time.LocalDate;

@ExtendWith(ApplicationExtension.class)
class SalaryTableControllerTest extends GuiTest {
    @Start
    void setUpGui(Stage stage) throws IOException {
        Salary salary = new Salary(LocalDate.parse("2020-01-01"),
                "salary1",
                "desc1",
                100);
        SalaryService.addSalary(salary);

        GuiTest.setupStage(stage, SalaryTableController.class);
    }

    @AfterEach
    void tearDown() {
        truncateDataBase();
    }

    @Test
    void shouldGoToSalaryAddViewWhenAddButtonGetsPressed(FxRobot robot) {
        robot.clickOn("Add");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Add Salary"));
    }

    @Test
    void shouldGoToSalaryEditViewWhenEditButtonGetsPressed(FxRobot robot) {
        robot.clickOn("salary1");
        robot.clickOn("#editButton");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Edit Salary"));
    }

    @Test
    void shouldReloadSalariesTableViewWhenDeleteButtonGetsPressed(FxRobot robot) {
        robot.clickOn("salary1");
        robot.clickOn("Delete");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Salaries"));
        assertThatNodeIsNotOnScreen("salary1", robot);
    }
}