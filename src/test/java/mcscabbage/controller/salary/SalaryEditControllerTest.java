package mcscabbage.controller.salary;

import javafx.stage.Stage;
import mcscabbage.controller.GuiTest;
import mcscabbage.entity.Salary;
import mcscabbage.service.SalaryService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.matcher.control.LabeledMatchers;

import java.io.IOException;
import java.time.LocalDate;

@ExtendWith(ApplicationExtension.class)
class SalaryEditControllerTest extends GuiTest {
    @Start
    void setUpGui(Stage stage) throws IOException {
        Salary salary = new Salary(LocalDate.parse("2020-01-01"),
                "salary1",
                "desc1",
                100);
        SalaryService.addSalary(salary);
        setupStage(stage, SalaryEditController.class, SalaryService.getSalaries().get(0));
    }

    @AfterEach
    void tearDown() {
        truncateDataBase();
    }

    @Test
    void shouldGoToSalariesTableViewWhenSaveButtonGetsPressed(FxRobot robot) {
        robot.clickOn("Save");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Salaries"));
    }

    @Test
    void shouldGoToSalariesTableViewWhenBackButtonGetsPressed(FxRobot robot) {
        robot.clickOn("Back");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Salaries"));
    }
}