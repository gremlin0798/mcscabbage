package mcscabbage.controller;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mcscabbage.controller.common.preferences.Preferences;
import mcscabbage.controller.common.viewrouter.FXMLView;
import mcscabbage.controller.common.viewrouter.FXMLViewWithParameter;
import mcscabbage.controller.common.viewrouter.ViewRouter;
import mcscabbage.service.*;
import org.testfx.api.FxRobot;
import org.testfx.api.FxRobotException;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuiTest {

    public static void setupStage(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(ViewRouter.class.getResource("/gui/menu/Menu.fxml"));
        fxmlLoader.setResources(ResourceBundle.getBundle("bundle/language", Locale.ENGLISH));

        Scene scene = new Scene(fxmlLoader.load(), 1280, 720);
        stage.setTitle("MCSCabbage");
        stage.setScene(scene);
        stage.show();
        stage.toFront();
    }

    public static <T extends FXMLView> void setupStage(Stage stage, Class<T> view) throws IOException {

        FXMLLoader fxmlLoader = new FXMLLoader(ViewRouter.class.getResource("/gui/menu/Menu.fxml"));
        fxmlLoader.setResources(ResourceBundle.getBundle("bundle/language", setPreferencesToDefaultValues().getLocale()));

        Scene scene = new Scene(fxmlLoader.load(), 1280, 720);
        stage.setTitle("MCSCabbage");
        stage.setScene(scene);
        stage.show();
        stage.toFront();

        ViewRouter.loadView(view);
    }

    public static <T extends FXMLView> void setupDialog(Stage stage, Class<T> dialog) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(ViewRouter.class.getResource(FXMLView.generateResourcePath(dialog)));
        fxmlLoader.setResources(ResourceBundle.getBundle("bundle/language", setPreferencesToDefaultValues().getLocale()));
        Scene scene = new Scene(fxmlLoader.load(), 300, 200);
        stage.setScene(scene);
        stage.show();
        stage.toFront();
    }

    public static <T extends FXMLViewWithParameter<E>, E> void setupStage(Stage stage, Class<T> view, E data) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(ViewRouter.class.getResource("/gui/menu/Menu.fxml"));
        fxmlLoader.setResources(ResourceBundle.getBundle("bundle/language", setPreferencesToDefaultValues().getLocale()));

        Scene scene = new Scene(fxmlLoader.load(), 1280, 720);
        stage.setTitle("MCSCabbage");
        stage.setScene(scene);
        stage.show();
        stage.toFront();

        ViewRouter.loadView(view, data);
    }

    public static Preferences setPreferencesToDefaultValues() {
        Preferences preferences = new Preferences();
        preferences.loadDefault();
        preferences.save();

        return preferences;
    }

    public static void assertThatNodeIsNotOnScreen(String query, FxRobot robot) {
        try {
            robot.clickOn(query);
        } catch (FxRobotException e) {
            assertEquals("the query \"" + query + "\" returned no nodes.", e.getMessage());
        }
    }

    public static void truncateDataBase() {
        EventService.getEvents().forEach(EventService::deleteEvent);
        RoleService.getRoles().forEach(RoleService::deleteRole);
        CompanyService.getCompanies().forEach(CompanyService::deleteCompany);
        PlaceService.getPlaces().forEach(PlaceService::deletePlace);
        SalaryService.getSalaries().forEach(SalaryService::deleteSalary);
        ExpenseService.getExpenses().forEach(ExpenseService::deleteExpense);
    }
}
