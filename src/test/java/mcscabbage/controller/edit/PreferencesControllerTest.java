package mcscabbage.controller.edit;

import javafx.stage.Stage;
import mcscabbage.controller.GuiTest;
import mcscabbage.controller.common.preferences.Preferences;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;

import java.io.IOException;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(ApplicationExtension.class)
class PreferencesControllerTest {

    @Start
    void setUp(Stage stage) throws IOException {
        GuiTest.setupDialog(stage, PreferencesController.class);
    }

    @Test
    void shouldSaveChangesWhenSaveButtonGetsPressed(FxRobot robot) {
        robot.clickOn("en");
        robot.clickOn("pl");
        robot.clickOn("Save");

        Preferences preferences = new Preferences();
        preferences.load();

        assertEquals(new Locale("pl"), preferences.getLocale());
    }

    @Test
    void shouldCloseWindowWhenBackButtonGetsPressed(FxRobot robot) {
        robot.clickOn("Back");

        GuiTest.assertThatNodeIsNotOnScreen("title", robot);
    }

}