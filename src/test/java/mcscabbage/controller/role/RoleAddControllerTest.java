package mcscabbage.controller.role;

import javafx.stage.Stage;
import mcscabbage.controller.GuiTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.matcher.control.LabeledMatchers;

import java.io.IOException;

@ExtendWith(ApplicationExtension.class)
class RoleAddControllerTest extends GuiTest {
    @Start
    void setUpGui(Stage stage) throws IOException {
        GuiTest.setupStage(stage, RoleAddController.class);
    }

    @AfterEach
    void tearDown() {
        truncateDataBase();
    }

    @Test
    void shouldGoToRolesTableViewWhenAddButtonGetsPressed(FxRobot robot) {
        robot.clickOn("#nameInputField");
        robot.write("role1");
        robot.clickOn("Add");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Roles"));
    }

    @Test
    void shouldGoToRolesTableViewWhenBackButtonGetsPressed(FxRobot robot) {
        robot.clickOn("Back");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Roles"));
    }

}