package mcscabbage.controller.role;

import javafx.stage.Stage;
import mcscabbage.controller.GuiTest;
import mcscabbage.entity.Role;
import mcscabbage.service.RoleService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.matcher.control.LabeledMatchers;

import java.io.IOException;

@ExtendWith(ApplicationExtension.class)
class RoleEditControllerTest extends GuiTest {
    @Start
    void setUpGui(Stage stage) throws IOException {
        RoleService.addRole(new Role("role1"));

        GuiTest.setupStage(stage, RoleEditController.class, RoleService.getRoles().get(0));
    }

    @AfterEach
    void tearDown() {
        truncateDataBase();
    }

    @Test
    void shouldGoToRolesTableViewWhenSaveButtonGetsPressed(FxRobot robot) {
        robot.clickOn("Save");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Roles"));
    }

    @Test
    void shouldGoToRolesTableViewWhenBackButtonGetsPressed(FxRobot robot) {
        robot.clickOn("Back");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Roles"));
    }

}