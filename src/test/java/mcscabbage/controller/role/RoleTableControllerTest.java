package mcscabbage.controller.role;

import javafx.stage.Stage;
import mcscabbage.controller.GuiTest;
import mcscabbage.entity.Role;
import mcscabbage.service.RoleService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.matcher.control.LabeledMatchers;

import java.io.IOException;

@ExtendWith(ApplicationExtension.class)
class RoleTableControllerTest extends GuiTest {
    @Start
    void setUpGui(Stage stage) throws IOException {
        RoleService.addRole(new Role("role1"));

        setupStage(stage, RoleTableController.class);
    }

    @AfterEach
    void tearDown() {
        truncateDataBase();
    }

    @Test
    void shouldGoToRoleAddViewWhenAddButtonGetsPressed(FxRobot robot) {
        robot.clickOn("Add");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Add Role"));
    }

    @Test
    void shouldGoToRoleEditViewWhenEditButtonGetsPressed(FxRobot robot) {
        robot.clickOn("role1");
        robot.clickOn("#editButton");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Edit Role"));
    }

    @Test
    void shouldReloadTableViewWhenDeleteButtonGetsPressed(FxRobot robot) {
        robot.clickOn("role1");
        robot.clickOn("Delete");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Roles"));
        assertThatNodeIsNotOnScreen("role1", robot);
    }

}