package mcscabbage.controller.expense;

import javafx.stage.Stage;
import mcscabbage.controller.GuiTest;
import mcscabbage.entity.Expense;
import mcscabbage.service.ExpenseService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.matcher.control.LabeledMatchers;

import java.io.IOException;
import java.time.LocalDate;

@ExtendWith(ApplicationExtension.class)
class ExpenseEditControllerTest extends GuiTest {
    @Start
    void setUpGui(Stage stage) throws IOException {
        Expense expense = new Expense(
                LocalDate.parse("2020-01-01"),
                "name1",
                "desc1",
                100
        );
        ExpenseService.addExpense(expense);
        setupStage(stage, ExpenseEditController.class, ExpenseService.getExpenses().stream().findFirst().get());
    }

    @AfterEach
    void tearDown() {
        truncateDataBase();
    }

    @Test
    void shouldGoToExpensesTableViewWhenSaveButtonGetsPressed(FxRobot robot) {
        robot.clickOn("Save");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Expenses"));
    }

    @Test
    void shouldGoToExpensesTableViewWhenBackButtonGetsPressed(FxRobot robot) {
        robot.clickOn("Back");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Expenses"));
    }
}