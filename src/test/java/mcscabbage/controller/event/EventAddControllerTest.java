package mcscabbage.controller.event;

import javafx.scene.control.DatePicker;
import javafx.stage.Stage;
import mcscabbage.controller.GuiTest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.matcher.control.LabeledMatchers;

import java.io.IOException;
import java.time.LocalDate;

@ExtendWith(ApplicationExtension.class)
class EventAddControllerTest extends GuiTest {
    @Start
    void setUpGui(Stage stage) throws IOException {
        GuiTest.setupStage(stage, EventAddController.class);
    }

    @AfterEach
    void tearDown() {
        truncateDataBase();
    }

    @Test
    void shouldGoToEventsTableViewWhenAddButtonGetsPressed(FxRobot robot) {
        robot.lookup("#datePicker").queryAs(DatePicker.class).setValue(LocalDate.parse("2020-01-01"));
        robot.clickOn("#nameInputField");
        robot.write("event1");
        robot.clickOn("Add");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Events"));
    }

    @Test
    void shouldGoToSalaryTableViewWhenBackButtonGetPressed(FxRobot robot) {
        robot.clickOn("Back");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Events"));
    }
}