package mcscabbage.controller.event;

import javafx.stage.Stage;
import mcscabbage.controller.GuiTest;
import mcscabbage.entity.Event;
import mcscabbage.service.EventService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.matcher.control.LabeledMatchers;

import java.io.IOException;
import java.time.LocalDate;

@ExtendWith(ApplicationExtension.class)
class EventEditControllerTest extends GuiTest {
    @Start
    void setUpGui(Stage stage) throws IOException {
        Event event = new Event(LocalDate.parse("2020-01-01"),
                "event1",
                null,
                null,
                null,
                100,
                "desc1");
        EventService.addEvent(event);
        setupStage(stage, EventEditController.class, EventService.getEvents().get(0));
    }

    @AfterEach
    void tearDown() {
        truncateDataBase();
    }

    @Test
    void shouldGoToEventsTableViewWhenSaveButtonGetsPressed(FxRobot robot) {
        robot.clickOn("Save");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Events"));
    }

    @Test
    void shouldGoToEventsTableViewWhenBackButtonGetsPressed(FxRobot robot) {
        robot.clickOn("Back");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Events"));
    }
}