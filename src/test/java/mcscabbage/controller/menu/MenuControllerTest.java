package mcscabbage.controller.menu;

import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import mcscabbage.controller.GuiTest;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxAssert;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.testfx.matcher.control.LabeledMatchers;

import java.io.IOException;

@Tag("GuiTest")
@ExtendWith(ApplicationExtension.class)
class MenuControllerTest {

    @Start
    void setupGui(Stage stage) throws IOException {
        GuiTest.setupStage(stage);
    }

    @Test
    void shouldGoToEventTableViewWhenEventsMenuItemGetsPressed(FxRobot robot) {
        robot.clickOn("View");
        robot.clickOn("Events");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Events"));
    }

    @Test
    void shouldGoToExpensesTableViewWhenExpensesMenuItemGetsPressed(FxRobot robot) {
        robot.clickOn("View");
        robot.clickOn("Expenses");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Expenses"));
    }

    @Test
    void shouldGoToSalariesTableViewWhenSalariesMenuItemGetsPressed(FxRobot robot) {
        robot.clickOn("View");
        robot.clickOn("Salaries");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Salaries"));
    }

    @Test
    void shouldGoToPlacesTableViewWhenPlacesMenuItemGetsPressed(FxRobot robot) {
        robot.clickOn("View");
        robot.clickOn("Places");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Places"));
    }

    @Test
    void shouldGoToRolesTableViewWhenRolesMenuItemGetsPressed(FxRobot robot) {
        robot.clickOn("View");
        robot.clickOn("Roles");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Roles"));
    }

    @Test
    void shouldGoToCompaniesTableViewWhenCompaniesMenuItemGetsPressed(FxRobot robot) {
        robot.clickOn("View");
        robot.clickOn("Companies");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Companies"));
    }

    @Test
    void shouldGoToMonthlySummaryViewWhenSummaryMenuItemGetsPressed(FxRobot robot) {
        robot.clickOn("Report");
        robot.clickOn("Summary");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Summary"));
    }

    @Test
    void shouldGoToDetailsViewWhenDetailsMenuItemGetsPressed(FxRobot robot) {
        robot.clickOn("Report");
        robot.clickOn("Details");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Details"));
    }

    @Test
    void shouldGoToExcelReportGeneratorWhenExcelReportMenuItemGetsPressed(FxRobot robot) {
        robot.clickOn("Report");
        robot.clickOn("Generate");
        robot.clickOn("Excel file (.xlsx)");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Create excel report"));
    }

    @Test
    void whenImportFromCSVMenuItemGetsPressedShouldGoToThatView(FxRobot robot) {
        robot.clickOn("File");
        robot.clickOn("Import");
        robot.clickOn("Table from CSV file");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Import from CSV file"));
    }

    @Test
    void whenExportToCSVMenuItemGetsPressedShouldGoToThatView(FxRobot robot) {
        robot.clickOn("File");
        robot.clickOn("Export");
        robot.clickOn("Table to CSV file");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Export to CSV file"));
    }

    @Test
    void shouldShowModalDialogWhenPreferencesMenuItemGetsPressed(FxRobot robot) {
        robot.clickOn("Edit");
        robot.clickOn("Preferences");

        FxAssert.verifyThat("#title", LabeledMatchers.hasText("Preferences"));
        robot.push(KeyCode.ALT, KeyCode.F4);

    }

}