package mcscabbage.controller.common.viewrouter;

import mcscabbage.controller.event.EventTableController;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FXMLViewTest {
    @Test
    void ShouldGenerateValidResourcePath() {
        assertEquals("/gui/event/EventTable.fxml", FXMLView.generateResourcePath(EventTableController.class));
    }

}