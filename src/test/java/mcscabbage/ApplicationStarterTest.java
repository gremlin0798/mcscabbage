package mcscabbage;

import mcscabbage.common.DatabaseUtil;
import mcscabbage.common.HibernateUtil;

class ApplicationStarterTest {
    public static void main(String[] args) {
        HibernateUtil.getSessionFactory();
        DatabaseUtil.setUpDB();
        FrontendStarter.start();
        HibernateUtil.shutdown();
    }
}