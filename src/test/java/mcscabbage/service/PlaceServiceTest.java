package mcscabbage.service;

import mcscabbage.common.HibernateUtil;
import mcscabbage.entity.Place;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PlaceServiceTest {
    @BeforeEach
    void setUp(){
        HibernateUtil.getSessionFactory();
    }

    @AfterAll
    static void tearDown(){
        HibernateUtil.shutdown();
    }
    @Test
    void shouldAllowAddPlacesWithPolishLetter(){
        Place place1 = new Place("żźćłęąó", "desc1");

        PlaceService.addPlace(place1);

        assertEquals("żźćłęąó", PlaceService.getPlaces().get(0).getName());
    }
}