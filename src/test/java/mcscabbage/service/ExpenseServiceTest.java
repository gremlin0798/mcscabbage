package mcscabbage.service;

import mcscabbage.common.HibernateUtil;
import mcscabbage.entity.Expense;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ExpenseServiceTest {

    @BeforeEach
    void setUp(){
        HibernateUtil.getSessionFactory();
    }

    @AfterAll
    static void tearDown(){
        HibernateUtil.shutdown();
    }

    @Test
    void shouldAllowAddExpenses() {
        Expense expense1 = new Expense(LocalDate.parse("2020-01-20"), "expense1", "desc1", 1.1);
        Expense expense2 = new Expense(LocalDate.parse("2020-01-21"), "expense2", "desc2", 2.2);

        ExpenseService.addExpense(expense1);
        ExpenseService.addExpense(expense2);

        assertEquals(2, ExpenseService.getExpenses().size());
    }

}
