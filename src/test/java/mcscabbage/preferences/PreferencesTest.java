package mcscabbage.preferences;

import mcscabbage.controller.common.preferences.Preferences;
import mcscabbage.exceptions.InvalidPreferenceValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class PreferencesTest {
    private static final String FILE_NAME = "preferences.cfg.bin";

    @BeforeEach
    void setUp() {
        File file = new File(FILE_NAME);
        file.delete();
    }

    @Test
    void shouldLoadDefaultValuesIfFileDoNotExist() {
        File file = new File(FILE_NAME);
        file.delete();

        Preferences expected = new Preferences();
        expected.loadDefault();

        Preferences preferences = new Preferences();
        preferences.load();

        assertEquals(expected, preferences);
    }

    @Test
    void shouldLoadValuesFromFileIfExist() throws InvalidPreferenceValue {
        Preferences expected = new Preferences();
        expected.setLanguage(Locale.ENGLISH);
        expected.save();

        Preferences preferences = new Preferences();
        preferences.load();

        assertEquals(expected, preferences);
    }

    @Test
    void shouldThrowExceptionWhenInvalidLocaleValueIsSet() {
        Preferences preferences = new Preferences();

        assertThrows(InvalidPreferenceValue.class, () -> preferences.setLanguage(Locale.CANADA));
    }
}