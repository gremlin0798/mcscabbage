package mcscabbage.entity;

import mcscabbage.exceptions.InvalidCSVFormat;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlaceTest {
    private static final String CSV1 = "name1,desc1";
    private static final Place PLACE1 = new Place("name1", "desc1");
    private static final String CSV2 = "name1,";
    private static final Place PLACE2 = new Place("name1", "");

    @Test
    void shouldCreateValidSCV() {
        assertAll(
                () -> assertEquals(CSV1, PLACE1.toCSVLine()),
                () -> assertEquals(CSV2, PLACE2.toCSVLine())
        );
    }

    @Test
    void shouldThrowExceptionForInvalidCSV() {
        Place place = new Place();
        assertThrows(InvalidCSVFormat.class, () -> place.fromCSVLine(CSV1 + ",inv"));
    }

    @Test
    void shouldCreateValidObjectFromCVS() throws InvalidCSVFormat {
        Place place1 = new Place();
        Place place2 = new Place();
        place1.fromCSVLine(CSV1);
        place2.fromCSVLine(CSV2);
        assertAll(
                () -> assertEquals(PLACE1, place1),
                () -> assertEquals(PLACE2, place2)

        );
    }

}