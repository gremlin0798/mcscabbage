package mcscabbage.entity;

import mcscabbage.exceptions.InvalidCSVFormat;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RoleTest {
    private static final String CSV = "name1";
    private static final Role ROLE = new Role("name1");

    @Test
    void shouldCreateValidSCV() {
        assertEquals(CSV, ROLE.toCSVLine());
    }

    @Test
    void shouldThrowExceptionForInvalidCSV() {
        Role role = new Role();
        assertThrows(InvalidCSVFormat.class, () -> role.fromCSVLine("name,inv"));
    }

    @Test
    void shouldCreateValidObjectFromCVS() throws InvalidCSVFormat {
        Role role = new Role();
        role.fromCSVLine(CSV);
        assertEquals(ROLE, role);
    }
}