package mcscabbage.entity;

import mcscabbage.exceptions.InvalidCSVFormat;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class SalaryTest {
    private static final String CSV1 = "2020-01-01,name1,desc1,50.0";
    private static final Salary SALARY1 = new Salary(
            LocalDate.parse("2020-01-01"),
            "name1",
            "desc1",
            50.0);
    private static final String CSV2 = "2020-01-01,name1,,50.0";
    private static final Salary SALARY2 = new Salary(
            LocalDate.parse("2020-01-01"),
            "name1",
            "",
            50.0);

    @Test
    void shouldCreateValidSCV() {
        assertAll(
                () -> assertEquals(CSV1, SALARY1.toCSVLine()),
                () -> assertEquals(CSV2, SALARY2.toCSVLine())
        );
    }

    @Test
    void shouldThrowExceptionForInvalidCSV() {
        Salary salary = new Salary();
        assertThrows(InvalidCSVFormat.class, () -> salary.fromCSVLine(CSV1 + ",inv"));
    }

    @Test
    void shouldCreateValidObjectFromCVS() throws InvalidCSVFormat {
        Salary salary1 = new Salary();
        Salary salary2 = new Salary();
        salary1.fromCSVLine(CSV1);
        salary2.fromCSVLine(CSV2);
        assertAll(
                () -> assertEquals(SALARY1, salary1),
                () -> assertEquals(SALARY2, salary2)

        );
    }

}