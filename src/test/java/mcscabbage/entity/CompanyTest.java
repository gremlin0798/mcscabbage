package mcscabbage.entity;

import mcscabbage.exceptions.InvalidCSVFormat;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CompanyTest {
    private static final String CSV = "name1";
    private static final Company COMPANY = new Company("name1");

    @Test
    void shouldCreateValidSCV() {
        assertEquals(CSV, COMPANY.toCSVLine());
    }

    @Test
    void shouldThrowExceptionForInvalidCSV() {
        Company company = new Company();
        assertThrows(InvalidCSVFormat.class, () -> company.fromCSVLine("name,inv"));
    }

    @Test
    void shouldCreateValidObjectFromCVS() throws InvalidCSVFormat {
        Company company = new Company();
        company.fromCSVLine(CSV);
        assertEquals(COMPANY, company);
    }
}