package mcscabbage.entity;

import mcscabbage.exceptions.InvalidCSVFormat;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class EventTest {
    private static final String CSV1 = "2020-01-01,name1,place1,company1,role1,50.0,desc1";
    private static final Event EVENT1 = new Event(
            LocalDate.parse("2020-01-01"),
            "name1",
            new Place("place1", ""),
            new Company("company1"),
            new Role("role1"),
            50.0,
            "desc1");
    private static final String CSV2 = "2020-01-01,name1,,,,50.0,desc1";
    private static final Event EVENT2 = new Event(
            LocalDate.parse("2020-01-01"),
            "name1",
            null,
            null,
            null,
            50.0,
            "desc1");

    @Test
    void shouldCreateValidSCV() {
        assertAll(
                () -> assertEquals(CSV1, EVENT1.toCSVLine()),
                () -> assertEquals(CSV2, EVENT2.toCSVLine())
        );
    }

    @Test
    void shouldThrowExceptionForInvalidCSV() {
        Event event = new Event();
        assertThrows(InvalidCSVFormat.class, () -> event.fromCSVLine(CSV1 + ",inv"));
    }

    @Test
    void shouldCreateValidObjectFromCVS() throws InvalidCSVFormat {
        Event event1 = new Event();
        Event event2 = new Event();
        event1.fromCSVLine(CSV1);
        event2.fromCSVLine(CSV2);
        assertAll(
                () -> assertEquals(EVENT1, event1),
                () -> assertEquals(EVENT2, event2)
        );
    }
}