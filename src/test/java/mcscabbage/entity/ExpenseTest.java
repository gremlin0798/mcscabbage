package mcscabbage.entity;

import mcscabbage.exceptions.InvalidCSVFormat;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class ExpenseTest {
    private static final String CSV1 = "2020-01-01,name1,desc1,50.0";
    private static final Expense EXPENSE1 = new Expense(
            LocalDate.parse("2020-01-01"),
            "name1",
            "desc1",
            50.0);
    private static final String CSV2 = "2020-01-01,name1,,50.0";
    private static final Expense EXPENSE2 = new Expense(
            LocalDate.parse("2020-01-01"),
            "name1",
            "",
            50.0);

    @Test
    void shouldCreateValidSCV() {
        assertAll(
                () -> assertEquals(CSV1, EXPENSE1.toCSVLine()),
                () -> assertEquals(CSV2, EXPENSE2.toCSVLine())
        );
    }

    @Test
    void shouldThrowExceptionForInvalidCSV() {
        Expense expense = new Expense();
        assertThrows(InvalidCSVFormat.class, () -> expense.fromCSVLine(CSV1 + ",inv"));
    }

    @Test
    void shouldCreateValidObjectFromCVS() throws InvalidCSVFormat {
        Expense expense1 = new Expense();
        Expense expense2 = new Expense();
        expense1.fromCSVLine(CSV1);
        expense2.fromCSVLine(CSV2);
        assertAll(
                () -> assertEquals(EXPENSE1, expense1),
                () -> assertEquals(EXPENSE2, expense2)

        );
    }
}