# MCSCabbage
Application was created to simplify process of generating monthly salary reports for company management. Repository is using Bitbucket Pipeline for continuous deployment, compiled files are uploaded to [Downloads](https://bitbucket.org/gremlin0798/mcscabbage/downloads/) section. User interface is tested with [TestFX](https://github.com/TestFX/TestFX).


## Features
* generating reports saved in excel files;
* showing salary statistic;
* import and export data to/from *.CSV file;
* data is saved to local H2 DB file

## Database ERD
![Database ERD](/screenshots/ERD.png =250x)

## Screenshots
![Event table view](/screenshots/eventTable.png =250x)
![Event edit view](/screenshots/eventEdit.png =250x)
![Summary view](/screenshots/monthlySummary.png =250x)
## Getting started
To build and run the application you will need JDK 1.8.

### From source code
To run it, simply enter the project directory and execute:

  * on Linux
```
$ ./gradlew run
```
  * on Windows
```
$ ./gradlew.exe run
```

### From compiled files

  * on Linux
```
$ cd MCSCabbage-*/bin
$ ./MCSCabbage
```
  * on Windows

Go to folder MCSCabbage-*\bin and run MCSCabbage.bin

## Dependencies
  * [H2 database](https://github.com/h2database/h2database)
  * [Junit 5](https://github.com/junit-team/junit5)
  * [Lombok](https://github.com/rzwitserloot/lombok)
  * [TestFX](https://github.com/TestFX/TestFX)
